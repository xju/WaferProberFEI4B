# Introduction
The code is written in the [Qt framework](https://www.qt.io/),
that supports the cross-platform develop.
It controls the probe station used at LBNL for testing the FE-I4B chips. 

# Installation
Please make sure Qt is install properly in the machine.
There are two versions of the code, one for Qt 4.xx and 
the other for Qt 5.xx. The difference between them is 
very small, but I think it's better to have them separted.

Download the Qt [link](https://www.qt.io/download-open-source/?hsCtaTracking=f977210e-de67-475f-a32b-65cec207fd03%7Cd62710cd-e1db-46aa-8d4d-2f1c1ffdacea).

Clone the source code to a clean local directory
`git clone https://gitlab.cern.ch/xju/WaferProberFEI4B`

The commands that controls the probe station 
are sent via the serial port, the interface of 
which is implemented by the 
[Qextserialport](https://github.com/qextserialport/qextserialport).
So please clone the following repository into the foler of 
either _qt5_ or _qt4_,
depending on the Qt version.

```unix
git clone https://github.com/qextserialport/qextserialport
```

If you are using the Qt Creator, just import the project and run.
Alternative, command lines can be used:
```unix
qmake WaferProber.pro
make
./WaferProber
```
