/********************************************************************************
** Form generated from reading UI file 'wp.ui'
**
** Created: Wed May 30 12:08:41 2012
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WP_H
#define UI_WP_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDoubleSpinBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPlainTextEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QStatusBar>
#include <QtGui/QTabWidget>
#include <QtGui/QToolBar>
#include <QtGui/QTreeWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WP
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_8;
    QHBoxLayout *horizontalLayout_13;
    QVBoxLayout *verticalLayout_4;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QLabel *inputHistoryLabel;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_3;
    QLabel *probeStationOutputLabel;
    QSpacerItem *horizontalSpacer_4;
    QPlainTextEdit *inputHistoryLog;
    QHBoxLayout *horizontalLayout_3;
    QLabel *enterCommandLabel;
    QLineEdit *commandLine;
    QPushButton *sendCommandButton;
    QPlainTextEdit *outputHistoryLog;
    QGridLayout *gridLayout_2;
    QSpacerItem *horizontalSpacer_9;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_5;
    QLabel *XAxisLabel;
    QSpacerItem *horizontalSpacer_6;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_7;
    QLabel *YAxisLabel;
    QSpacerItem *horizontalSpacer_8;
    QHBoxLayout *horizontalLayout_12;
    QSpacerItem *horizontalSpacer_20;
    QLabel *label_18;
    QSpacerItem *horizontalSpacer_21;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_10;
    QLabel *ZAxisLabel;
    QSpacerItem *horizontalSpacer_11;
    QPushButton *moveRelativeButton;
    QLineEdit *moveRelativeX;
    QLineEdit *moveRelativeY;
    QHBoxLayout *horizontalLayout_8;
    QSpacerItem *horizontalSpacer_14;
    QPushButton *moveZButton;
    QSpacerItem *horizontalSpacer_15;
    QPushButton *moveIncrementButton;
    QLineEdit *moveIncrementX;
    QLineEdit *moveIncrementY;
    QGroupBox *otherMovementFunctionsBox;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_16;
    QSpacerItem *horizontalSpacer_17;
    QVBoxLayout *verticalLayout_5;
    QPushButton *moveHomeButton;
    QPushButton *setHomeButton;
    QPushButton *setContactButton;
    QPushButton *setSeparateButton;
    QSpacerItem *horizontalSpacer_19;
    QSpacerItem *verticalSpacer_2;
    QPushButton *setIncrementButton;
    QLineEdit *setIncrementX;
    QLineEdit *setIncrementY;
    QLabel *currentIncrementLabel;
    QLineEdit *currentXIncrementBox;
    QLineEdit *currentYIncrementBox;
    QLabel *currentLocationLabel;
    QLineEdit *currentXLocationBox;
    QLineEdit *currentYLocationBox;
    QHBoxLayout *horizontalLayout_11;
    QSpacerItem *horizontalSpacer_13;
    QLabel *columnLabel;
    QLineEdit *currentColumnBox;
    QHBoxLayout *horizontalLayout_7;
    QSpacerItem *horizontalSpacer_12;
    QLabel *rowLabel;
    QLineEdit *currentRowBox;
    QLabel *currentChipLabel;
    QLineEdit *currentChipBox;
    QPushButton *moveToChipButton;
    QLineEdit *moveToChipBox;
    QLineEdit *currentTLocationBox;
    QTabWidget *tabWidget;
    QWidget *proberInfoTab;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_17;
    QSpacerItem *horizontalSpacer_28;
    QPushButton *connectProberButton;
    QSpacerItem *horizontalSpacer_29;
    QGridLayout *gridLayout_7;
    QLabel *label_3;
    QComboBox *baudRateBox;
    QLabel *label_13;
    QComboBox *parityBox;
    QLabel *label_14;
    QLineEdit *deviceNameBox;
    QLabel *label_15;
    QComboBox *stopBitsBox;
    QLabel *label_16;
    QComboBox *dataBitsBox;
    QLabel *label_17;
    QComboBox *flowControlBox;
    QHBoxLayout *horizontalLayout_20;
    QSpacerItem *horizontalSpacer_26;
    QPushButton *setConnectionButton;
    QSpacerItem *horizontalSpacer_27;
    QHBoxLayout *horizontalLayout_22;
    QSpacerItem *horizontalSpacer_32;
    QPushButton *peekButton;
    QPushButton *readBufferButton;
    QSpacerItem *horizontalSpacer_33;
    QSpacerItem *verticalSpacer_14;
    QWidget *otherFunctionsTab;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_9;
    QPushButton *clearInputHistoryButton;
    QPushButton *clearOutputHistoryButton;
    QPushButton *clearMsgBox;
    QHBoxLayout *horizontalLayout_10;
    QSpacerItem *horizontalSpacer_16;
    QPushButton *refreshLocationButton;
    QSpacerItem *horizontalSpacer_18;
    QHBoxLayout *horizontalLayout_15;
    QSpacerItem *horizontalSpacer_24;
    QPushButton *resetIncrementButton;
    QSpacerItem *horizontalSpacer_25;
    QHBoxLayout *horizontalLayout_21;
    QSpacerItem *horizontalSpacer_30;
    QPushButton *enableStartupToolsButton;
    QSpacerItem *horizontalSpacer_31;
    QGroupBox *startupToolsGroup;
    QGridLayout *gridLayout_10;
    QGridLayout *gridLayout_4;
    QPushButton *positionReportingOffButton;
    QPushButton *setTVeloButton;
    QPushButton *setXVeloButton;
    QPushButton *setYVeloButton;
    QPushButton *initializeButton;
    QPushButton *setUnitsButton;
    QComboBox *unitsBox;
    QPushButton *setWaferSizeButton;
    QDoubleSpinBox *tVeloBox;
    QDoubleSpinBox *xVeloBox;
    QDoubleSpinBox *yVeloBox;
    QDoubleSpinBox *waferSizeBox;
    QSpacerItem *verticalSpacer;
    QWidget *waferTab;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout_2;
    QGridLayout *gridLayout_3;
    QPushButton *alignButton1;
    QGridLayout *gridLayout_8;
    QLabel *label_7;
    QLineEdit *alignLocationXBox1;
    QLabel *label_8;
    QLineEdit *alignLocationYBox1;
    QLabel *label_9;
    QLineEdit *alignLocationTBox1;
    QSpacerItem *verticalSpacer_3;
    QSpacerItem *verticalSpacer_4;
    QHBoxLayout *horizontalLayout_18;
    QPushButton *alignButton2;
    QLabel *label;
    QLineEdit *alignMoveIncrementXBox;
    QSpacerItem *verticalSpacer_5;
    QGridLayout *gridLayout_16;
    QPushButton *alignButton3;
    QGridLayout *gridLayout_5;
    QLabel *label_4;
    QLineEdit *alignLocationXBox2;
    QLabel *label_5;
    QLineEdit *alignLocationYBox2;
    QLabel *label_6;
    QLineEdit *alignLocationTBox2;
    QSpacerItem *verticalSpacer_6;
    QSpacerItem *verticalSpacer_9;
    QGridLayout *gridLayout_18;
    QPushButton *alignButton4;
    QGridLayout *gridLayout_17;
    QLabel *label_2;
    QLineEdit *alignMoveRelativeXBox;
    QLabel *label_23;
    QLineEdit *alignMoveRelativeYBox;
    QSpacerItem *verticalSpacer_7;
    QSpacerItem *verticalSpacer_10;
    QGridLayout *gridLayout_19;
    QPushButton *alignButton5;
    QGridLayout *gridLayout_9;
    QLabel *label_10;
    QLineEdit *alignLocationXBox3;
    QLabel *label_11;
    QLineEdit *alignLocationYBox3;
    QLabel *label_12;
    QLineEdit *alignLocationTBox3;
    QSpacerItem *verticalSpacer_8;
    QSpacerItem *verticalSpacer_11;
    QHBoxLayout *horizontalLayout_19;
    QPushButton *alignButton6;
    QLineEdit *alignComputeRotationBox;
    QPushButton *alignButton7;
    QSpacerItem *verticalSpacer_12;
    QHBoxLayout *horizontalLayout_14;
    QSpacerItem *horizontalSpacer_22;
    QLabel *alignmentLabel;
    QSpacerItem *horizontalSpacer_23;
    QSpacerItem *verticalSpacer_13;
    QWidget *libraryTab;
    QGridLayout *gridLayout_6;
    QTreeWidget *treeWidget;
    QPlainTextEdit *msgBox;
    QMenuBar *menuBar;
    QMenu *menuVersion_5_0;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *WP)
    {
        if (WP->objectName().isEmpty())
            WP->setObjectName(QString::fromUtf8("WP"));
        WP->setEnabled(true);
        WP->resize(1050, 868);
        centralWidget = new QWidget(WP);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout_8 = new QVBoxLayout(centralWidget);
        verticalLayout_8->setSpacing(6);
        verticalLayout_8->setContentsMargins(11, 11, 11, 11);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setSpacing(6);
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        inputHistoryLabel = new QLabel(centralWidget);
        inputHistoryLabel->setObjectName(QString::fromUtf8("inputHistoryLabel"));

        horizontalLayout_2->addWidget(inputHistoryLabel);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        gridLayout->addLayout(horizontalLayout_2, 0, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);

        probeStationOutputLabel = new QLabel(centralWidget);
        probeStationOutputLabel->setObjectName(QString::fromUtf8("probeStationOutputLabel"));

        horizontalLayout->addWidget(probeStationOutputLabel);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);


        gridLayout->addLayout(horizontalLayout, 0, 1, 1, 1);

        inputHistoryLog = new QPlainTextEdit(centralWidget);
        inputHistoryLog->setObjectName(QString::fromUtf8("inputHistoryLog"));
        inputHistoryLog->setMinimumSize(QSize(295, 201));
        inputHistoryLog->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        gridLayout->addWidget(inputHistoryLog, 1, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        enterCommandLabel = new QLabel(centralWidget);
        enterCommandLabel->setObjectName(QString::fromUtf8("enterCommandLabel"));

        horizontalLayout_3->addWidget(enterCommandLabel);

        commandLine = new QLineEdit(centralWidget);
        commandLine->setObjectName(QString::fromUtf8("commandLine"));

        horizontalLayout_3->addWidget(commandLine);

        sendCommandButton = new QPushButton(centralWidget);
        sendCommandButton->setObjectName(QString::fromUtf8("sendCommandButton"));
        sendCommandButton->setMinimumSize(QSize(82, 23));
        sendCommandButton->setMaximumSize(QSize(82, 23));
        sendCommandButton->setAutoDefault(false);
        sendCommandButton->setDefault(false);

        horizontalLayout_3->addWidget(sendCommandButton);


        gridLayout->addLayout(horizontalLayout_3, 2, 0, 1, 2);

        outputHistoryLog = new QPlainTextEdit(centralWidget);
        outputHistoryLog->setObjectName(QString::fromUtf8("outputHistoryLog"));
        outputHistoryLog->setMinimumSize(QSize(294, 201));
        outputHistoryLog->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        gridLayout->addWidget(outputHistoryLog, 1, 1, 1, 1);


        verticalLayout_4->addLayout(gridLayout);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        horizontalSpacer_9 = new QSpacerItem(88, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_9, 0, 0, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_5);

        XAxisLabel = new QLabel(centralWidget);
        XAxisLabel->setObjectName(QString::fromUtf8("XAxisLabel"));

        horizontalLayout_4->addWidget(XAxisLabel);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_6);


        gridLayout_2->addLayout(horizontalLayout_4, 0, 1, 1, 1);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalSpacer_7 = new QSpacerItem(40, 18, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_7);

        YAxisLabel = new QLabel(centralWidget);
        YAxisLabel->setObjectName(QString::fromUtf8("YAxisLabel"));

        horizontalLayout_5->addWidget(YAxisLabel);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_8);


        gridLayout_2->addLayout(horizontalLayout_5, 0, 2, 1, 1);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setSpacing(6);
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        horizontalSpacer_20 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_12->addItem(horizontalSpacer_20);

        label_18 = new QLabel(centralWidget);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        horizontalLayout_12->addWidget(label_18);

        horizontalSpacer_21 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_12->addItem(horizontalSpacer_21);


        gridLayout_2->addLayout(horizontalLayout_12, 0, 3, 1, 1);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_10);

        ZAxisLabel = new QLabel(centralWidget);
        ZAxisLabel->setObjectName(QString::fromUtf8("ZAxisLabel"));

        horizontalLayout_6->addWidget(ZAxisLabel);

        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_11);


        gridLayout_2->addLayout(horizontalLayout_6, 0, 4, 1, 1);

        moveRelativeButton = new QPushButton(centralWidget);
        moveRelativeButton->setObjectName(QString::fromUtf8("moveRelativeButton"));
        moveRelativeButton->setMinimumSize(QSize(93, 23));
        moveRelativeButton->setMaximumSize(QSize(93, 23));

        gridLayout_2->addWidget(moveRelativeButton, 1, 0, 1, 1);

        moveRelativeX = new QLineEdit(centralWidget);
        moveRelativeX->setObjectName(QString::fromUtf8("moveRelativeX"));

        gridLayout_2->addWidget(moveRelativeX, 1, 1, 1, 1);

        moveRelativeY = new QLineEdit(centralWidget);
        moveRelativeY->setObjectName(QString::fromUtf8("moveRelativeY"));

        gridLayout_2->addWidget(moveRelativeY, 1, 2, 1, 1);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        horizontalSpacer_14 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_14);

        moveZButton = new QPushButton(centralWidget);
        moveZButton->setObjectName(QString::fromUtf8("moveZButton"));
        moveZButton->setMinimumSize(QSize(124, 23));
        moveZButton->setMaximumSize(QSize(124, 23));

        horizontalLayout_8->addWidget(moveZButton);

        horizontalSpacer_15 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_15);


        gridLayout_2->addLayout(horizontalLayout_8, 1, 4, 1, 1);

        moveIncrementButton = new QPushButton(centralWidget);
        moveIncrementButton->setObjectName(QString::fromUtf8("moveIncrementButton"));
        moveIncrementButton->setMinimumSize(QSize(93, 23));
        moveIncrementButton->setMaximumSize(QSize(93, 23));

        gridLayout_2->addWidget(moveIncrementButton, 2, 0, 1, 1);

        moveIncrementX = new QLineEdit(centralWidget);
        moveIncrementX->setObjectName(QString::fromUtf8("moveIncrementX"));

        gridLayout_2->addWidget(moveIncrementX, 2, 1, 1, 1);

        moveIncrementY = new QLineEdit(centralWidget);
        moveIncrementY->setObjectName(QString::fromUtf8("moveIncrementY"));

        gridLayout_2->addWidget(moveIncrementY, 2, 2, 1, 1);

        otherMovementFunctionsBox = new QGroupBox(centralWidget);
        otherMovementFunctionsBox->setObjectName(QString::fromUtf8("otherMovementFunctionsBox"));
        verticalLayout_6 = new QVBoxLayout(otherMovementFunctionsBox);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setSpacing(6);
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        horizontalSpacer_17 = new QSpacerItem(18, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_16->addItem(horizontalSpacer_17);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        moveHomeButton = new QPushButton(otherMovementFunctionsBox);
        moveHomeButton->setObjectName(QString::fromUtf8("moveHomeButton"));
        moveHomeButton->setMinimumSize(QSize(91, 23));
        moveHomeButton->setMaximumSize(QSize(91, 23));

        verticalLayout_5->addWidget(moveHomeButton);

        setHomeButton = new QPushButton(otherMovementFunctionsBox);
        setHomeButton->setObjectName(QString::fromUtf8("setHomeButton"));
        setHomeButton->setMinimumSize(QSize(91, 23));
        setHomeButton->setMaximumSize(QSize(91, 23));

        verticalLayout_5->addWidget(setHomeButton);

        setContactButton = new QPushButton(otherMovementFunctionsBox);
        setContactButton->setObjectName(QString::fromUtf8("setContactButton"));
        setContactButton->setMinimumSize(QSize(91, 23));
        setContactButton->setMaximumSize(QSize(91, 23));

        verticalLayout_5->addWidget(setContactButton);

        setSeparateButton = new QPushButton(otherMovementFunctionsBox);
        setSeparateButton->setObjectName(QString::fromUtf8("setSeparateButton"));
        setSeparateButton->setMinimumSize(QSize(91, 23));
        setSeparateButton->setMaximumSize(QSize(91, 23));

        verticalLayout_5->addWidget(setSeparateButton);


        horizontalLayout_16->addLayout(verticalLayout_5);

        horizontalSpacer_19 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_16->addItem(horizontalSpacer_19);


        verticalLayout_6->addLayout(horizontalLayout_16);

        verticalSpacer_2 = new QSpacerItem(20, 31, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(verticalSpacer_2);


        gridLayout_2->addWidget(otherMovementFunctionsBox, 2, 4, 7, 1);

        setIncrementButton = new QPushButton(centralWidget);
        setIncrementButton->setObjectName(QString::fromUtf8("setIncrementButton"));
        setIncrementButton->setMinimumSize(QSize(93, 23));
        setIncrementButton->setMaximumSize(QSize(93, 23));

        gridLayout_2->addWidget(setIncrementButton, 3, 0, 1, 1);

        setIncrementX = new QLineEdit(centralWidget);
        setIncrementX->setObjectName(QString::fromUtf8("setIncrementX"));

        gridLayout_2->addWidget(setIncrementX, 3, 1, 1, 1);

        setIncrementY = new QLineEdit(centralWidget);
        setIncrementY->setObjectName(QString::fromUtf8("setIncrementY"));

        gridLayout_2->addWidget(setIncrementY, 3, 2, 1, 1);

        currentIncrementLabel = new QLabel(centralWidget);
        currentIncrementLabel->setObjectName(QString::fromUtf8("currentIncrementLabel"));

        gridLayout_2->addWidget(currentIncrementLabel, 4, 0, 1, 1);

        currentXIncrementBox = new QLineEdit(centralWidget);
        currentXIncrementBox->setObjectName(QString::fromUtf8("currentXIncrementBox"));
        currentXIncrementBox->setReadOnly(true);

        gridLayout_2->addWidget(currentXIncrementBox, 4, 1, 1, 1);

        currentYIncrementBox = new QLineEdit(centralWidget);
        currentYIncrementBox->setObjectName(QString::fromUtf8("currentYIncrementBox"));
        currentYIncrementBox->setReadOnly(true);

        gridLayout_2->addWidget(currentYIncrementBox, 4, 2, 1, 1);

        currentLocationLabel = new QLabel(centralWidget);
        currentLocationLabel->setObjectName(QString::fromUtf8("currentLocationLabel"));

        gridLayout_2->addWidget(currentLocationLabel, 5, 0, 1, 1);

        currentXLocationBox = new QLineEdit(centralWidget);
        currentXLocationBox->setObjectName(QString::fromUtf8("currentXLocationBox"));
        currentXLocationBox->setReadOnly(true);

        gridLayout_2->addWidget(currentXLocationBox, 5, 1, 1, 1);

        currentYLocationBox = new QLineEdit(centralWidget);
        currentYLocationBox->setObjectName(QString::fromUtf8("currentYLocationBox"));
        currentYLocationBox->setReadOnly(true);

        gridLayout_2->addWidget(currentYLocationBox, 5, 2, 1, 1);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        horizontalSpacer_13 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_13);

        columnLabel = new QLabel(centralWidget);
        columnLabel->setObjectName(QString::fromUtf8("columnLabel"));

        horizontalLayout_11->addWidget(columnLabel);

        currentColumnBox = new QLineEdit(centralWidget);
        currentColumnBox->setObjectName(QString::fromUtf8("currentColumnBox"));
        currentColumnBox->setMinimumSize(QSize(81, 20));
        currentColumnBox->setMaximumSize(QSize(81, 20));
        currentColumnBox->setReadOnly(true);

        horizontalLayout_11->addWidget(currentColumnBox);


        gridLayout_2->addLayout(horizontalLayout_11, 6, 1, 1, 1);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_12);

        rowLabel = new QLabel(centralWidget);
        rowLabel->setObjectName(QString::fromUtf8("rowLabel"));

        horizontalLayout_7->addWidget(rowLabel);

        currentRowBox = new QLineEdit(centralWidget);
        currentRowBox->setObjectName(QString::fromUtf8("currentRowBox"));
        currentRowBox->setMinimumSize(QSize(81, 20));
        currentRowBox->setMaximumSize(QSize(81, 20));
        currentRowBox->setReadOnly(true);

        horizontalLayout_7->addWidget(currentRowBox);


        gridLayout_2->addLayout(horizontalLayout_7, 6, 2, 1, 1);

        currentChipLabel = new QLabel(centralWidget);
        currentChipLabel->setObjectName(QString::fromUtf8("currentChipLabel"));

        gridLayout_2->addWidget(currentChipLabel, 7, 0, 1, 1);

        currentChipBox = new QLineEdit(centralWidget);
        currentChipBox->setObjectName(QString::fromUtf8("currentChipBox"));
        currentChipBox->setMinimumSize(QSize(0, 20));
        currentChipBox->setMaximumSize(QSize(271, 20));
        currentChipBox->setReadOnly(true);

        gridLayout_2->addWidget(currentChipBox, 7, 1, 1, 1);

        moveToChipButton = new QPushButton(centralWidget);
        moveToChipButton->setObjectName(QString::fromUtf8("moveToChipButton"));
        moveToChipButton->setMinimumSize(QSize(75, 23));
        moveToChipButton->setMaximumSize(QSize(91, 23));

        gridLayout_2->addWidget(moveToChipButton, 8, 0, 1, 1);

        moveToChipBox = new QLineEdit(centralWidget);
        moveToChipBox->setObjectName(QString::fromUtf8("moveToChipBox"));
        moveToChipBox->setMinimumSize(QSize(0, 20));
        moveToChipBox->setMaximumSize(QSize(271, 20));

        gridLayout_2->addWidget(moveToChipBox, 8, 1, 1, 1);

        currentTLocationBox = new QLineEdit(centralWidget);
        currentTLocationBox->setObjectName(QString::fromUtf8("currentTLocationBox"));

        gridLayout_2->addWidget(currentTLocationBox, 5, 3, 1, 1);


        verticalLayout_4->addLayout(gridLayout_2);


        horizontalLayout_13->addLayout(verticalLayout_4);

        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setMinimumSize(QSize(341, 731));
        proberInfoTab = new QWidget();
        proberInfoTab->setObjectName(QString::fromUtf8("proberInfoTab"));
        verticalLayout_7 = new QVBoxLayout(proberInfoTab);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setSpacing(6);
        horizontalLayout_17->setObjectName(QString::fromUtf8("horizontalLayout_17"));
        horizontalSpacer_28 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_17->addItem(horizontalSpacer_28);

        connectProberButton = new QPushButton(proberInfoTab);
        connectProberButton->setObjectName(QString::fromUtf8("connectProberButton"));
        connectProberButton->setMinimumSize(QSize(151, 40));
        connectProberButton->setMaximumSize(QSize(151, 40));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        connectProberButton->setFont(font);

        horizontalLayout_17->addWidget(connectProberButton);

        horizontalSpacer_29 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_17->addItem(horizontalSpacer_29);


        verticalLayout_7->addLayout(horizontalLayout_17);

        gridLayout_7 = new QGridLayout();
        gridLayout_7->setSpacing(6);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        label_3 = new QLabel(proberInfoTab);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_7->addWidget(label_3, 0, 0, 1, 1);

        baudRateBox = new QComboBox(proberInfoTab);
        baudRateBox->setObjectName(QString::fromUtf8("baudRateBox"));

        gridLayout_7->addWidget(baudRateBox, 0, 1, 1, 1);

        label_13 = new QLabel(proberInfoTab);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        gridLayout_7->addWidget(label_13, 1, 0, 1, 1);

        parityBox = new QComboBox(proberInfoTab);
        parityBox->setObjectName(QString::fromUtf8("parityBox"));

        gridLayout_7->addWidget(parityBox, 1, 1, 1, 1);

        label_14 = new QLabel(proberInfoTab);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        gridLayout_7->addWidget(label_14, 2, 0, 1, 1);

        deviceNameBox = new QLineEdit(proberInfoTab);
        deviceNameBox->setObjectName(QString::fromUtf8("deviceNameBox"));

        gridLayout_7->addWidget(deviceNameBox, 2, 1, 1, 1);

        label_15 = new QLabel(proberInfoTab);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        gridLayout_7->addWidget(label_15, 3, 0, 1, 1);

        stopBitsBox = new QComboBox(proberInfoTab);
        stopBitsBox->setObjectName(QString::fromUtf8("stopBitsBox"));

        gridLayout_7->addWidget(stopBitsBox, 3, 1, 1, 1);

        label_16 = new QLabel(proberInfoTab);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        gridLayout_7->addWidget(label_16, 4, 0, 1, 1);

        dataBitsBox = new QComboBox(proberInfoTab);
        dataBitsBox->setObjectName(QString::fromUtf8("dataBitsBox"));

        gridLayout_7->addWidget(dataBitsBox, 4, 1, 1, 1);

        label_17 = new QLabel(proberInfoTab);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        gridLayout_7->addWidget(label_17, 5, 0, 1, 1);

        flowControlBox = new QComboBox(proberInfoTab);
        flowControlBox->setObjectName(QString::fromUtf8("flowControlBox"));

        gridLayout_7->addWidget(flowControlBox, 5, 1, 1, 1);


        verticalLayout_7->addLayout(gridLayout_7);

        horizontalLayout_20 = new QHBoxLayout();
        horizontalLayout_20->setSpacing(6);
        horizontalLayout_20->setObjectName(QString::fromUtf8("horizontalLayout_20"));
        horizontalSpacer_26 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_20->addItem(horizontalSpacer_26);

        setConnectionButton = new QPushButton(proberInfoTab);
        setConnectionButton->setObjectName(QString::fromUtf8("setConnectionButton"));
        setConnectionButton->setMinimumSize(QSize(141, 23));
        setConnectionButton->setMaximumSize(QSize(141, 23));

        horizontalLayout_20->addWidget(setConnectionButton);

        horizontalSpacer_27 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_20->addItem(horizontalSpacer_27);


        verticalLayout_7->addLayout(horizontalLayout_20);

        horizontalLayout_22 = new QHBoxLayout();
        horizontalLayout_22->setSpacing(6);
        horizontalLayout_22->setObjectName(QString::fromUtf8("horizontalLayout_22"));
        horizontalSpacer_32 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_22->addItem(horizontalSpacer_32);

        peekButton = new QPushButton(proberInfoTab);
        peekButton->setObjectName(QString::fromUtf8("peekButton"));

        horizontalLayout_22->addWidget(peekButton);

        readBufferButton = new QPushButton(proberInfoTab);
        readBufferButton->setObjectName(QString::fromUtf8("readBufferButton"));

        horizontalLayout_22->addWidget(readBufferButton);

        horizontalSpacer_33 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_22->addItem(horizontalSpacer_33);


        verticalLayout_7->addLayout(horizontalLayout_22);

        verticalSpacer_14 = new QSpacerItem(20, 460, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer_14);

        tabWidget->addTab(proberInfoTab, QString());
        otherFunctionsTab = new QWidget();
        otherFunctionsTab->setObjectName(QString::fromUtf8("otherFunctionsTab"));
        verticalLayout = new QVBoxLayout(otherFunctionsTab);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        clearInputHistoryButton = new QPushButton(otherFunctionsTab);
        clearInputHistoryButton->setObjectName(QString::fromUtf8("clearInputHistoryButton"));
        clearInputHistoryButton->setMinimumSize(QSize(118, 23));
        clearInputHistoryButton->setMaximumSize(QSize(118, 23));

        horizontalLayout_9->addWidget(clearInputHistoryButton);

        clearOutputHistoryButton = new QPushButton(otherFunctionsTab);
        clearOutputHistoryButton->setObjectName(QString::fromUtf8("clearOutputHistoryButton"));
        clearOutputHistoryButton->setMinimumSize(QSize(118, 23));
        clearOutputHistoryButton->setMaximumSize(QSize(118, 23));

        horizontalLayout_9->addWidget(clearOutputHistoryButton);

        clearMsgBox = new QPushButton(otherFunctionsTab);
        clearMsgBox->setObjectName(QString::fromUtf8("clearMsgBox"));

        horizontalLayout_9->addWidget(clearMsgBox);


        verticalLayout->addLayout(horizontalLayout_9);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        horizontalSpacer_16 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_16);

        refreshLocationButton = new QPushButton(otherFunctionsTab);
        refreshLocationButton->setObjectName(QString::fromUtf8("refreshLocationButton"));
        refreshLocationButton->setMinimumSize(QSize(118, 23));
        refreshLocationButton->setMaximumSize(QSize(118, 23));

        horizontalLayout_10->addWidget(refreshLocationButton);

        horizontalSpacer_18 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_18);


        verticalLayout->addLayout(horizontalLayout_10);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setSpacing(6);
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        horizontalSpacer_24 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_15->addItem(horizontalSpacer_24);

        resetIncrementButton = new QPushButton(otherFunctionsTab);
        resetIncrementButton->setObjectName(QString::fromUtf8("resetIncrementButton"));

        horizontalLayout_15->addWidget(resetIncrementButton);

        horizontalSpacer_25 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_15->addItem(horizontalSpacer_25);


        verticalLayout->addLayout(horizontalLayout_15);

        horizontalLayout_21 = new QHBoxLayout();
        horizontalLayout_21->setSpacing(6);
        horizontalLayout_21->setObjectName(QString::fromUtf8("horizontalLayout_21"));
        horizontalSpacer_30 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_21->addItem(horizontalSpacer_30);

        enableStartupToolsButton = new QPushButton(otherFunctionsTab);
        enableStartupToolsButton->setObjectName(QString::fromUtf8("enableStartupToolsButton"));

        horizontalLayout_21->addWidget(enableStartupToolsButton);

        horizontalSpacer_31 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_21->addItem(horizontalSpacer_31);


        verticalLayout->addLayout(horizontalLayout_21);

        startupToolsGroup = new QGroupBox(otherFunctionsTab);
        startupToolsGroup->setObjectName(QString::fromUtf8("startupToolsGroup"));
        startupToolsGroup->setEnabled(false);
        gridLayout_10 = new QGridLayout(startupToolsGroup);
        gridLayout_10->setSpacing(6);
        gridLayout_10->setContentsMargins(11, 11, 11, 11);
        gridLayout_10->setObjectName(QString::fromUtf8("gridLayout_10"));
        gridLayout_4 = new QGridLayout();
        gridLayout_4->setSpacing(6);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        positionReportingOffButton = new QPushButton(startupToolsGroup);
        positionReportingOffButton->setObjectName(QString::fromUtf8("positionReportingOffButton"));

        gridLayout_4->addWidget(positionReportingOffButton, 0, 0, 1, 2);

        setTVeloButton = new QPushButton(startupToolsGroup);
        setTVeloButton->setObjectName(QString::fromUtf8("setTVeloButton"));

        gridLayout_4->addWidget(setTVeloButton, 1, 0, 1, 1);

        setXVeloButton = new QPushButton(startupToolsGroup);
        setXVeloButton->setObjectName(QString::fromUtf8("setXVeloButton"));

        gridLayout_4->addWidget(setXVeloButton, 2, 0, 1, 1);

        setYVeloButton = new QPushButton(startupToolsGroup);
        setYVeloButton->setObjectName(QString::fromUtf8("setYVeloButton"));

        gridLayout_4->addWidget(setYVeloButton, 3, 0, 1, 1);

        initializeButton = new QPushButton(startupToolsGroup);
        initializeButton->setObjectName(QString::fromUtf8("initializeButton"));

        gridLayout_4->addWidget(initializeButton, 4, 0, 1, 2);

        setUnitsButton = new QPushButton(startupToolsGroup);
        setUnitsButton->setObjectName(QString::fromUtf8("setUnitsButton"));

        gridLayout_4->addWidget(setUnitsButton, 5, 0, 1, 1);

        unitsBox = new QComboBox(startupToolsGroup);
        unitsBox->setObjectName(QString::fromUtf8("unitsBox"));

        gridLayout_4->addWidget(unitsBox, 5, 1, 1, 1);

        setWaferSizeButton = new QPushButton(startupToolsGroup);
        setWaferSizeButton->setObjectName(QString::fromUtf8("setWaferSizeButton"));

        gridLayout_4->addWidget(setWaferSizeButton, 6, 0, 1, 1);

        tVeloBox = new QDoubleSpinBox(startupToolsGroup);
        tVeloBox->setObjectName(QString::fromUtf8("tVeloBox"));
        tVeloBox->setValue(1);

        gridLayout_4->addWidget(tVeloBox, 1, 1, 1, 1);

        xVeloBox = new QDoubleSpinBox(startupToolsGroup);
        xVeloBox->setObjectName(QString::fromUtf8("xVeloBox"));
        xVeloBox->setValue(4);

        gridLayout_4->addWidget(xVeloBox, 2, 1, 1, 1);

        yVeloBox = new QDoubleSpinBox(startupToolsGroup);
        yVeloBox->setObjectName(QString::fromUtf8("yVeloBox"));
        yVeloBox->setValue(4);

        gridLayout_4->addWidget(yVeloBox, 3, 1, 1, 1);

        waferSizeBox = new QDoubleSpinBox(startupToolsGroup);
        waferSizeBox->setObjectName(QString::fromUtf8("waferSizeBox"));
        waferSizeBox->setMaximum(200);
        waferSizeBox->setValue(200);

        gridLayout_4->addWidget(waferSizeBox, 6, 1, 1, 1);


        gridLayout_10->addLayout(gridLayout_4, 0, 0, 1, 1);


        verticalLayout->addWidget(startupToolsGroup);

        verticalSpacer = new QSpacerItem(17, 560, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        tabWidget->addTab(otherFunctionsTab, QString());
        waferTab = new QWidget();
        waferTab->setObjectName(QString::fromUtf8("waferTab"));
        verticalLayout_3 = new QVBoxLayout(waferTab);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        alignButton1 = new QPushButton(waferTab);
        alignButton1->setObjectName(QString::fromUtf8("alignButton1"));
        alignButton1->setMinimumSize(QSize(121, 23));
        alignButton1->setMaximumSize(QSize(121, 23));

        gridLayout_3->addWidget(alignButton1, 0, 0, 1, 1);

        gridLayout_8 = new QGridLayout();
        gridLayout_8->setSpacing(6);
        gridLayout_8->setObjectName(QString::fromUtf8("gridLayout_8"));
        label_7 = new QLabel(waferTab);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout_8->addWidget(label_7, 0, 0, 1, 1);

        alignLocationXBox1 = new QLineEdit(waferTab);
        alignLocationXBox1->setObjectName(QString::fromUtf8("alignLocationXBox1"));
        alignLocationXBox1->setReadOnly(true);

        gridLayout_8->addWidget(alignLocationXBox1, 0, 1, 1, 1);

        label_8 = new QLabel(waferTab);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout_8->addWidget(label_8, 1, 0, 1, 1);

        alignLocationYBox1 = new QLineEdit(waferTab);
        alignLocationYBox1->setObjectName(QString::fromUtf8("alignLocationYBox1"));
        alignLocationYBox1->setReadOnly(true);

        gridLayout_8->addWidget(alignLocationYBox1, 1, 1, 1, 1);

        label_9 = new QLabel(waferTab);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout_8->addWidget(label_9, 2, 0, 1, 1);

        alignLocationTBox1 = new QLineEdit(waferTab);
        alignLocationTBox1->setObjectName(QString::fromUtf8("alignLocationTBox1"));
        alignLocationTBox1->setReadOnly(true);

        gridLayout_8->addWidget(alignLocationTBox1, 2, 1, 1, 1);


        gridLayout_3->addLayout(gridLayout_8, 0, 1, 2, 1);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_3->addItem(verticalSpacer_3, 1, 0, 1, 1);


        verticalLayout_2->addLayout(gridLayout_3);

        verticalSpacer_4 = new QSpacerItem(20, 13, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_4);

        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setSpacing(6);
        horizontalLayout_18->setObjectName(QString::fromUtf8("horizontalLayout_18"));
        alignButton2 = new QPushButton(waferTab);
        alignButton2->setObjectName(QString::fromUtf8("alignButton2"));
        alignButton2->setEnabled(false);
        alignButton2->setMinimumSize(QSize(121, 23));
        alignButton2->setMaximumSize(QSize(121, 23));

        horizontalLayout_18->addWidget(alignButton2);

        label = new QLabel(waferTab);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_18->addWidget(label);

        alignMoveIncrementXBox = new QLineEdit(waferTab);
        alignMoveIncrementXBox->setObjectName(QString::fromUtf8("alignMoveIncrementXBox"));

        horizontalLayout_18->addWidget(alignMoveIncrementXBox);


        verticalLayout_2->addLayout(horizontalLayout_18);

        verticalSpacer_5 = new QSpacerItem(20, 13, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_5);

        gridLayout_16 = new QGridLayout();
        gridLayout_16->setSpacing(6);
        gridLayout_16->setObjectName(QString::fromUtf8("gridLayout_16"));
        alignButton3 = new QPushButton(waferTab);
        alignButton3->setObjectName(QString::fromUtf8("alignButton3"));
        alignButton3->setEnabled(false);
        alignButton3->setMinimumSize(QSize(121, 23));
        alignButton3->setMaximumSize(QSize(121, 23));

        gridLayout_16->addWidget(alignButton3, 0, 0, 1, 1);

        gridLayout_5 = new QGridLayout();
        gridLayout_5->setSpacing(6);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        label_4 = new QLabel(waferTab);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout_5->addWidget(label_4, 0, 0, 1, 1);

        alignLocationXBox2 = new QLineEdit(waferTab);
        alignLocationXBox2->setObjectName(QString::fromUtf8("alignLocationXBox2"));
        alignLocationXBox2->setReadOnly(true);

        gridLayout_5->addWidget(alignLocationXBox2, 0, 1, 1, 1);

        label_5 = new QLabel(waferTab);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout_5->addWidget(label_5, 1, 0, 1, 1);

        alignLocationYBox2 = new QLineEdit(waferTab);
        alignLocationYBox2->setObjectName(QString::fromUtf8("alignLocationYBox2"));
        alignLocationYBox2->setReadOnly(true);

        gridLayout_5->addWidget(alignLocationYBox2, 1, 1, 1, 1);

        label_6 = new QLabel(waferTab);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout_5->addWidget(label_6, 2, 0, 1, 1);

        alignLocationTBox2 = new QLineEdit(waferTab);
        alignLocationTBox2->setObjectName(QString::fromUtf8("alignLocationTBox2"));
        alignLocationTBox2->setReadOnly(true);

        gridLayout_5->addWidget(alignLocationTBox2, 2, 1, 1, 1);


        gridLayout_16->addLayout(gridLayout_5, 0, 1, 2, 1);

        verticalSpacer_6 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_16->addItem(verticalSpacer_6, 1, 0, 1, 1);


        verticalLayout_2->addLayout(gridLayout_16);

        verticalSpacer_9 = new QSpacerItem(20, 13, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_9);

        gridLayout_18 = new QGridLayout();
        gridLayout_18->setSpacing(6);
        gridLayout_18->setObjectName(QString::fromUtf8("gridLayout_18"));
        alignButton4 = new QPushButton(waferTab);
        alignButton4->setObjectName(QString::fromUtf8("alignButton4"));
        alignButton4->setEnabled(false);
        alignButton4->setMinimumSize(QSize(121, 23));
        alignButton4->setMaximumSize(QSize(121, 23));

        gridLayout_18->addWidget(alignButton4, 0, 0, 1, 1);

        gridLayout_17 = new QGridLayout();
        gridLayout_17->setSpacing(6);
        gridLayout_17->setObjectName(QString::fromUtf8("gridLayout_17"));
        label_2 = new QLabel(waferTab);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_17->addWidget(label_2, 0, 0, 1, 1);

        alignMoveRelativeXBox = new QLineEdit(waferTab);
        alignMoveRelativeXBox->setObjectName(QString::fromUtf8("alignMoveRelativeXBox"));

        gridLayout_17->addWidget(alignMoveRelativeXBox, 0, 1, 1, 1);

        label_23 = new QLabel(waferTab);
        label_23->setObjectName(QString::fromUtf8("label_23"));

        gridLayout_17->addWidget(label_23, 1, 0, 1, 1);

        alignMoveRelativeYBox = new QLineEdit(waferTab);
        alignMoveRelativeYBox->setObjectName(QString::fromUtf8("alignMoveRelativeYBox"));

        gridLayout_17->addWidget(alignMoveRelativeYBox, 1, 1, 1, 1);


        gridLayout_18->addLayout(gridLayout_17, 0, 1, 2, 1);

        verticalSpacer_7 = new QSpacerItem(20, 18, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_18->addItem(verticalSpacer_7, 1, 0, 1, 1);


        verticalLayout_2->addLayout(gridLayout_18);

        verticalSpacer_10 = new QSpacerItem(20, 13, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_10);

        gridLayout_19 = new QGridLayout();
        gridLayout_19->setSpacing(6);
        gridLayout_19->setObjectName(QString::fromUtf8("gridLayout_19"));
        alignButton5 = new QPushButton(waferTab);
        alignButton5->setObjectName(QString::fromUtf8("alignButton5"));
        alignButton5->setEnabled(false);
        alignButton5->setMinimumSize(QSize(121, 23));
        alignButton5->setMaximumSize(QSize(121, 23));

        gridLayout_19->addWidget(alignButton5, 0, 0, 1, 1);

        gridLayout_9 = new QGridLayout();
        gridLayout_9->setSpacing(6);
        gridLayout_9->setObjectName(QString::fromUtf8("gridLayout_9"));
        label_10 = new QLabel(waferTab);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout_9->addWidget(label_10, 0, 0, 1, 1);

        alignLocationXBox3 = new QLineEdit(waferTab);
        alignLocationXBox3->setObjectName(QString::fromUtf8("alignLocationXBox3"));
        alignLocationXBox3->setReadOnly(true);

        gridLayout_9->addWidget(alignLocationXBox3, 0, 1, 1, 1);

        label_11 = new QLabel(waferTab);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout_9->addWidget(label_11, 1, 0, 1, 1);

        alignLocationYBox3 = new QLineEdit(waferTab);
        alignLocationYBox3->setObjectName(QString::fromUtf8("alignLocationYBox3"));
        alignLocationYBox3->setReadOnly(true);

        gridLayout_9->addWidget(alignLocationYBox3, 1, 1, 1, 1);

        label_12 = new QLabel(waferTab);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        gridLayout_9->addWidget(label_12, 2, 0, 1, 1);

        alignLocationTBox3 = new QLineEdit(waferTab);
        alignLocationTBox3->setObjectName(QString::fromUtf8("alignLocationTBox3"));
        alignLocationTBox3->setReadOnly(true);

        gridLayout_9->addWidget(alignLocationTBox3, 2, 1, 1, 1);


        gridLayout_19->addLayout(gridLayout_9, 0, 1, 2, 1);

        verticalSpacer_8 = new QSpacerItem(20, 28, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_19->addItem(verticalSpacer_8, 1, 0, 1, 1);


        verticalLayout_2->addLayout(gridLayout_19);

        verticalSpacer_11 = new QSpacerItem(20, 13, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_11);

        horizontalLayout_19 = new QHBoxLayout();
        horizontalLayout_19->setSpacing(6);
        horizontalLayout_19->setObjectName(QString::fromUtf8("horizontalLayout_19"));
        alignButton6 = new QPushButton(waferTab);
        alignButton6->setObjectName(QString::fromUtf8("alignButton6"));
        alignButton6->setEnabled(false);
        alignButton6->setMinimumSize(QSize(121, 23));
        alignButton6->setMaximumSize(QSize(121, 23));

        horizontalLayout_19->addWidget(alignButton6);

        alignComputeRotationBox = new QLineEdit(waferTab);
        alignComputeRotationBox->setObjectName(QString::fromUtf8("alignComputeRotationBox"));
        alignComputeRotationBox->setReadOnly(true);

        horizontalLayout_19->addWidget(alignComputeRotationBox);


        verticalLayout_2->addLayout(horizontalLayout_19);

        alignButton7 = new QPushButton(waferTab);
        alignButton7->setObjectName(QString::fromUtf8("alignButton7"));
        alignButton7->setEnabled(false);
        alignButton7->setMinimumSize(QSize(121, 23));
        alignButton7->setMaximumSize(QSize(121, 23));

        verticalLayout_2->addWidget(alignButton7);


        verticalLayout_3->addLayout(verticalLayout_2);

        verticalSpacer_12 = new QSpacerItem(20, 79, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_12);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setSpacing(6);
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        horizontalSpacer_22 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_14->addItem(horizontalSpacer_22);

        alignmentLabel = new QLabel(waferTab);
        alignmentLabel->setObjectName(QString::fromUtf8("alignmentLabel"));
        alignmentLabel->setMinimumSize(QSize(0, 0));

        horizontalLayout_14->addWidget(alignmentLabel);

        horizontalSpacer_23 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_14->addItem(horizontalSpacer_23);


        verticalLayout_3->addLayout(horizontalLayout_14);

        verticalSpacer_13 = new QSpacerItem(20, 78, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_13);

        tabWidget->addTab(waferTab, QString());
        libraryTab = new QWidget();
        libraryTab->setObjectName(QString::fromUtf8("libraryTab"));
        gridLayout_6 = new QGridLayout(libraryTab);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        treeWidget = new QTreeWidget(libraryTab);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem(treeWidget);
        QTreeWidgetItem *__qtreewidgetitem1 = new QTreeWidgetItem(__qtreewidgetitem);
        QTreeWidgetItem *__qtreewidgetitem2 = new QTreeWidgetItem(__qtreewidgetitem1);
        new QTreeWidgetItem(__qtreewidgetitem2);
        QTreeWidgetItem *__qtreewidgetitem3 = new QTreeWidgetItem(__qtreewidgetitem1);
        QTreeWidgetItem *__qtreewidgetitem4 = new QTreeWidgetItem(__qtreewidgetitem3);
        new QTreeWidgetItem(__qtreewidgetitem4);
        QTreeWidgetItem *__qtreewidgetitem5 = new QTreeWidgetItem(__qtreewidgetitem3);
        new QTreeWidgetItem(__qtreewidgetitem5);
        QTreeWidgetItem *__qtreewidgetitem6 = new QTreeWidgetItem(__qtreewidgetitem);
        QTreeWidgetItem *__qtreewidgetitem7 = new QTreeWidgetItem(__qtreewidgetitem6);
        new QTreeWidgetItem(__qtreewidgetitem7);
        QTreeWidgetItem *__qtreewidgetitem8 = new QTreeWidgetItem(__qtreewidgetitem6);
        new QTreeWidgetItem(__qtreewidgetitem8);
        QTreeWidgetItem *__qtreewidgetitem9 = new QTreeWidgetItem(__qtreewidgetitem);
        QTreeWidgetItem *__qtreewidgetitem10 = new QTreeWidgetItem(__qtreewidgetitem9);
        new QTreeWidgetItem(__qtreewidgetitem10);
        QTreeWidgetItem *__qtreewidgetitem11 = new QTreeWidgetItem(__qtreewidgetitem9);
        new QTreeWidgetItem(__qtreewidgetitem11);
        QTreeWidgetItem *__qtreewidgetitem12 = new QTreeWidgetItem(__qtreewidgetitem9);
        QTreeWidgetItem *__qtreewidgetitem13 = new QTreeWidgetItem(__qtreewidgetitem12);
        new QTreeWidgetItem(__qtreewidgetitem13);
        QTreeWidgetItem *__qtreewidgetitem14 = new QTreeWidgetItem(__qtreewidgetitem12);
        new QTreeWidgetItem(__qtreewidgetitem14);
        QTreeWidgetItem *__qtreewidgetitem15 = new QTreeWidgetItem(treeWidget);
        QTreeWidgetItem *__qtreewidgetitem16 = new QTreeWidgetItem(__qtreewidgetitem15);
        new QTreeWidgetItem(__qtreewidgetitem16);
        new QTreeWidgetItem(__qtreewidgetitem16);
        QTreeWidgetItem *__qtreewidgetitem17 = new QTreeWidgetItem(__qtreewidgetitem15);
        new QTreeWidgetItem(__qtreewidgetitem17);
        new QTreeWidgetItem(__qtreewidgetitem17);
        treeWidget->setObjectName(QString::fromUtf8("treeWidget"));
        treeWidget->setItemsExpandable(true);

        gridLayout_6->addWidget(treeWidget, 0, 0, 1, 1);

        tabWidget->addTab(libraryTab, QString());

        horizontalLayout_13->addWidget(tabWidget);


        verticalLayout_8->addLayout(horizontalLayout_13);

        msgBox = new QPlainTextEdit(centralWidget);
        msgBox->setObjectName(QString::fromUtf8("msgBox"));

        verticalLayout_8->addWidget(msgBox);

        WP->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(WP);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1050, 18));
        menuVersion_5_0 = new QMenu(menuBar);
        menuVersion_5_0->setObjectName(QString::fromUtf8("menuVersion_5_0"));
        WP->setMenuBar(menuBar);
        mainToolBar = new QToolBar(WP);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        WP->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(WP);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        WP->setStatusBar(statusBar);

        menuBar->addAction(menuVersion_5_0->menuAction());

        retranslateUi(WP);

        tabWidget->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(WP);
    } // setupUi

    void retranslateUi(QMainWindow *WP)
    {
        WP->setWindowTitle(QApplication::translate("WP", "WP", 0, QApplication::UnicodeUTF8));
        inputHistoryLabel->setText(QApplication::translate("WP", "Input History", 0, QApplication::UnicodeUTF8));
        probeStationOutputLabel->setText(QApplication::translate("WP", "Probe Station Output", 0, QApplication::UnicodeUTF8));
        enterCommandLabel->setText(QApplication::translate("WP", "Enter Command:", 0, QApplication::UnicodeUTF8));
        sendCommandButton->setText(QApplication::translate("WP", "Send Command", 0, QApplication::UnicodeUTF8));
        outputHistoryLog->setPlainText(QString());
        XAxisLabel->setText(QApplication::translate("WP", "X-Axis", 0, QApplication::UnicodeUTF8));
        YAxisLabel->setText(QApplication::translate("WP", "Y-Axis", 0, QApplication::UnicodeUTF8));
        label_18->setText(QApplication::translate("WP", "T-Axis", 0, QApplication::UnicodeUTF8));
        ZAxisLabel->setText(QApplication::translate("WP", "Z-Axis", 0, QApplication::UnicodeUTF8));
        moveRelativeButton->setText(QApplication::translate("WP", "Move Relative", 0, QApplication::UnicodeUTF8));
        moveZButton->setText(QApplication::translate("WP", "Move to Contact", 0, QApplication::UnicodeUTF8));
        moveIncrementButton->setText(QApplication::translate("WP", "Move Increment", 0, QApplication::UnicodeUTF8));
        otherMovementFunctionsBox->setTitle(QApplication::translate("WP", "Other Movement Functions", 0, QApplication::UnicodeUTF8));
        moveHomeButton->setText(QApplication::translate("WP", "Move Home", 0, QApplication::UnicodeUTF8));
        setHomeButton->setText(QApplication::translate("WP", "Set Home", 0, QApplication::UnicodeUTF8));
        setContactButton->setText(QApplication::translate("WP", "Set Contact", 0, QApplication::UnicodeUTF8));
        setSeparateButton->setText(QApplication::translate("WP", "Set Separate...", 0, QApplication::UnicodeUTF8));
        setIncrementButton->setText(QApplication::translate("WP", "Set Increment", 0, QApplication::UnicodeUTF8));
        currentIncrementLabel->setText(QApplication::translate("WP", "Current Increment:", 0, QApplication::UnicodeUTF8));
        currentLocationLabel->setText(QApplication::translate("WP", "Current Location: ", 0, QApplication::UnicodeUTF8));
        columnLabel->setText(QApplication::translate("WP", "Column: ", 0, QApplication::UnicodeUTF8));
        rowLabel->setText(QApplication::translate("WP", "Row:", 0, QApplication::UnicodeUTF8));
        currentChipLabel->setText(QApplication::translate("WP", "Current Chip:", 0, QApplication::UnicodeUTF8));
        moveToChipButton->setText(QApplication::translate("WP", "Move to Chip", 0, QApplication::UnicodeUTF8));
        connectProberButton->setText(QApplication::translate("WP", "Connect", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("WP", "Baud Rate:", 0, QApplication::UnicodeUTF8));
        label_13->setText(QApplication::translate("WP", "Parity:", 0, QApplication::UnicodeUTF8));
        label_14->setText(QApplication::translate("WP", "Device Name (COM Port):", 0, QApplication::UnicodeUTF8));
        label_15->setText(QApplication::translate("WP", "Stop Bits:", 0, QApplication::UnicodeUTF8));
        label_16->setText(QApplication::translate("WP", "Data Bits:", 0, QApplication::UnicodeUTF8));
        label_17->setText(QApplication::translate("WP", "Flow Control:", 0, QApplication::UnicodeUTF8));
        setConnectionButton->setText(QApplication::translate("WP", "Set Connection Properties", 0, QApplication::UnicodeUTF8));
        peekButton->setText(QApplication::translate("WP", "Peek", 0, QApplication::UnicodeUTF8));
        readBufferButton->setText(QApplication::translate("WP", "ReadBuffer", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(proberInfoTab), QApplication::translate("WP", "Prober", 0, QApplication::UnicodeUTF8));
        clearInputHistoryButton->setText(QApplication::translate("WP", "Clear Input History", 0, QApplication::UnicodeUTF8));
        clearOutputHistoryButton->setText(QApplication::translate("WP", "Clear Output History", 0, QApplication::UnicodeUTF8));
        clearMsgBox->setText(QApplication::translate("WP", "Clear Messages", 0, QApplication::UnicodeUTF8));
        refreshLocationButton->setText(QApplication::translate("WP", "Refresh Location", 0, QApplication::UnicodeUTF8));
        resetIncrementButton->setText(QApplication::translate("WP", "Reset Increment to FE-I4 Default", 0, QApplication::UnicodeUTF8));
        enableStartupToolsButton->setText(QApplication::translate("WP", "Enable Startup Configuration Tools", 0, QApplication::UnicodeUTF8));
        startupToolsGroup->setTitle(QApplication::translate("WP", "Startup Configuration Tools", 0, QApplication::UnicodeUTF8));
        positionReportingOffButton->setText(QApplication::translate("WP", "Turn Off Position Reporting", 0, QApplication::UnicodeUTF8));
        setTVeloButton->setText(QApplication::translate("WP", "Set \316\230 Velocity (Deg/s)", 0, QApplication::UnicodeUTF8));
        setXVeloButton->setText(QApplication::translate("WP", "Set X Velocity (mm/s)", 0, QApplication::UnicodeUTF8));
        setYVeloButton->setText(QApplication::translate("WP", "Set Y Velocity (mm/s)", 0, QApplication::UnicodeUTF8));
        initializeButton->setText(QApplication::translate("WP", "Send Initialization Command", 0, QApplication::UnicodeUTF8));
        setUnitsButton->setText(QApplication::translate("WP", "Set Units", 0, QApplication::UnicodeUTF8));
        setWaferSizeButton->setText(QApplication::translate("WP", "Set Wafer Size (mm)", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(otherFunctionsTab), QApplication::translate("WP", "Other Functions", 0, QApplication::UnicodeUTF8));
        alignButton1->setText(QApplication::translate("WP", "Get 1st Location", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("WP", "X:", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("WP", "Y:", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("WP", "T:", 0, QApplication::UnicodeUTF8));
        alignButton2->setText(QApplication::translate("WP", "Move to Other Side", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("WP", "X:", 0, QApplication::UnicodeUTF8));
        alignButton3->setText(QApplication::translate("WP", "Get 2nd Location", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("WP", "X:", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("WP", "Y:", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("WP", "T:", 0, QApplication::UnicodeUTF8));
        alignButton4->setText(QApplication::translate("WP", "Move Relative", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("WP", "X:", 0, QApplication::UnicodeUTF8));
        label_23->setText(QApplication::translate("WP", "Y:", 0, QApplication::UnicodeUTF8));
        alignButton5->setText(QApplication::translate("WP", "Get 3rd Location", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("WP", "X:", 0, QApplication::UnicodeUTF8));
        label_11->setText(QApplication::translate("WP", "Y:", 0, QApplication::UnicodeUTF8));
        label_12->setText(QApplication::translate("WP", "T:", 0, QApplication::UnicodeUTF8));
        alignButton6->setText(QApplication::translate("WP", "Compute Rotation \316\230", 0, QApplication::UnicodeUTF8));
        alignButton7->setText(QApplication::translate("WP", "Rotate DUT To Align", 0, QApplication::UnicodeUTF8));
        alignmentLabel->setText(QApplication::translate("WP", "Start alignment process by clicking Get 1st Location", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(waferTab), QApplication::translate("WP", "Align Wafer", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem = treeWidget->headerItem();
        ___qtreewidgetitem->setText(0, QApplication::translate("WP", "Library", 0, QApplication::UnicodeUTF8));

        const bool __sortingEnabled = treeWidget->isSortingEnabled();
        treeWidget->setSortingEnabled(false);
        QTreeWidgetItem *___qtreewidgetitem1 = treeWidget->topLevelItem(0);
        ___qtreewidgetitem1->setText(0, QApplication::translate("WP", "Prober Commands", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem2 = ___qtreewidgetitem1->child(0);
        ___qtreewidgetitem2->setText(0, QApplication::translate("WP", "Settings", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem3 = ___qtreewidgetitem2->child(0);
        ___qtreewidgetitem3->setText(0, QApplication::translate("WP", "Set Increment", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem4 = ___qtreewidgetitem3->child(0);
        ___qtreewidgetitem4->setText(0, QApplication::translate("WP", "SN D X [increment] Y [increment]", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        ___qtreewidgetitem4->setToolTip(0, QApplication::translate("WP", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">General form: SN &lt;channel&gt; [&lt;axis&gt; &lt;increment&gt; ...]</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Sets the amount the prober moves when Move Increment is used. Typically set to the chip-to-chip pitch.</span></p></body>"
                        "</html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        QTreeWidgetItem *___qtreewidgetitem5 = ___qtreewidgetitem2->child(1);
        ___qtreewidgetitem5->setText(0, QApplication::translate("WP", "Set Z Axis", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem6 = ___qtreewidgetitem5->child(0);
        ___qtreewidgetitem6->setText(0, QApplication::translate("WP", "Set Contact", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem7 = ___qtreewidgetitem6->child(0);
        ___qtreewidgetitem7->setText(0, QApplication::translate("WP", "SZ D C", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        ___qtreewidgetitem7->setToolTip(0, QApplication::translate("WP", "Sets the current z-axis position as the contact position. When the Move to Contact command is sent, the prober will move to the contact position unless it is already there.", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        QTreeWidgetItem *___qtreewidgetitem8 = ___qtreewidgetitem5->child(1);
        ___qtreewidgetitem8->setText(0, QApplication::translate("WP", "Set Separate", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem9 = ___qtreewidgetitem8->child(0);
        ___qtreewidgetitem9->setText(0, QApplication::translate("WP", "SZ D S [position (\302\265m)]", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        ___qtreewidgetitem9->setToolTip(0, QApplication::translate("WP", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Sets the current z-axis position as the separate position. When the Move to Separate command is sent, the prober will move to the separate position unless it is already there.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Additionally, "
                        "a position can be specified. Even though all other distances used by the prober are in mm, this position is in \302\265m, and it is always relative to the contact position.</span></p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        QTreeWidgetItem *___qtreewidgetitem10 = ___qtreewidgetitem1->child(1);
        ___qtreewidgetitem10->setText(0, QApplication::translate("WP", "Queries", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem11 = ___qtreewidgetitem10->child(0);
        ___qtreewidgetitem11->setText(0, QApplication::translate("WP", "Query Position", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem12 = ___qtreewidgetitem11->child(0);
        ___qtreewidgetitem12->setText(0, QApplication::translate("WP", "QP D", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem13 = ___qtreewidgetitem10->child(1);
        ___qtreewidgetitem13->setText(0, QApplication::translate("WP", "Query Increment", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem14 = ___qtreewidgetitem13->child(0);
        ___qtreewidgetitem14->setText(0, QApplication::translate("WP", "QN D", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem15 = ___qtreewidgetitem1->child(2);
        ___qtreewidgetitem15->setText(0, QApplication::translate("WP", "Movement", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem16 = ___qtreewidgetitem15->child(0);
        ___qtreewidgetitem16->setText(0, QApplication::translate("WP", "Move Relative", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem17 = ___qtreewidgetitem16->child(0);
        ___qtreewidgetitem17->setText(0, QApplication::translate("WP", "MR D X [distance] Y [distance]", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem18 = ___qtreewidgetitem15->child(1);
        ___qtreewidgetitem18->setText(0, QApplication::translate("WP", "Move Increment", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem19 = ___qtreewidgetitem18->child(0);
        ___qtreewidgetitem19->setText(0, QApplication::translate("WP", "MN D X [# of steps] Y [# of steps]", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem20 = ___qtreewidgetitem15->child(2);
        ___qtreewidgetitem20->setText(0, QApplication::translate("WP", "Move Z Axis", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem21 = ___qtreewidgetitem20->child(0);
        ___qtreewidgetitem21->setText(0, QApplication::translate("WP", "Move to Contact", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem22 = ___qtreewidgetitem21->child(0);
        ___qtreewidgetitem22->setText(0, QApplication::translate("WP", "MZ D C", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem23 = ___qtreewidgetitem20->child(1);
        ___qtreewidgetitem23->setText(0, QApplication::translate("WP", "Move to Separate", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem24 = ___qtreewidgetitem23->child(0);
        ___qtreewidgetitem24->setText(0, QApplication::translate("WP", "MZ D S", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem25 = treeWidget->topLevelItem(1);
        ___qtreewidgetitem25->setText(0, QApplication::translate("WP", "FE-I4 Information", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem26 = ___qtreewidgetitem25->child(0);
        ___qtreewidgetitem26->setText(0, QApplication::translate("WP", "Chip Pitch (Measured)", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem27 = ___qtreewidgetitem26->child(0);
        ___qtreewidgetitem27->setText(0, QApplication::translate("WP", "Chip to Chip Pitch X: 20.320228", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem28 = ___qtreewidgetitem26->child(1);
        ___qtreewidgetitem28->setText(0, QApplication::translate("WP", "Chip to Chip Pitch Y: 19.26256", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem29 = ___qtreewidgetitem25->child(1);
        ___qtreewidgetitem29->setText(0, QApplication::translate("WP", "Chip Pitch - Specified", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem30 = ___qtreewidgetitem29->child(0);
        ___qtreewidgetitem30->setText(0, QApplication::translate("WP", "Chip to Chip Pitch X: 20.33016", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem31 = ___qtreewidgetitem29->child(1);
        ___qtreewidgetitem31->setText(0, QApplication::translate("WP", "Chip to Chip Pitch Y: 19.26256", 0, QApplication::UnicodeUTF8));
        treeWidget->setSortingEnabled(__sortingEnabled);

        tabWidget->setTabText(tabWidget->indexOf(libraryTab), QApplication::translate("WP", "Library", 0, QApplication::UnicodeUTF8));
        menuVersion_5_0->setTitle(QApplication::translate("WP", "Version 5.0", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class WP: public Ui_WP {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WP_H
