#include "wp.h"
#include "ui_wp.h"

#define DEBUG true
#ifdef DEBUG
#include <iostream>
using namespace std;
#endif

WP::WP(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::WP)
{
    ui->setupUi(this);

    alignmentlatch = 0;
    m_separated_flag = true;					//Separated? Start out true.
    m_check_location_flag = true;					//Need to check location? Start out true.
    m_chip_pitch_x = 20.320228;					//The chip to chip pitches for FE-I4 are listed here.
    m_chip_pitch_y = 19.26256;
    //serialPort = new SerialPort();
    serialPort = new QextSerialPort(QextSerialPort::EventDriven, this);

    PI = acos(double(-1));					//Set pi.
    Msg(QString("Pi = %1").arg(PI));				//Display pi.

    connect(ui->sendCommandButton, SIGNAL(clicked()), this, SLOT(SendLineCommand()));
    connect(ui->commandLine, SIGNAL(returnPressed()), this, SLOT(SendLineCommand()));
    connect(ui->moveZButton, SIGNAL(clicked()), this, SLOT(MoveZ()));
    connect(ui->moveRelativeButton, SIGNAL(clicked()), this, SLOT(MoveRelative()));
    connect(ui->moveIncrementButton, SIGNAL(clicked()), this, SLOT(MoveIncrement()));
    connect(ui->moveIncrementX, SIGNAL(returnPressed()), this, SLOT(MoveIncrement()));
    connect(ui->moveIncrementY, SIGNAL(returnPressed()), this, SLOT(MoveIncrement()));
    connect(ui->setIncrementButton, SIGNAL(clicked()), this, SLOT(SetIncrement()));
    connect(ui->setIncrementX, SIGNAL(returnPressed()), this, SLOT(SetIncrement()));
    connect(ui->setIncrementY, SIGNAL(returnPressed()), this, SLOT(SetIncrement()));
    connect(ui->moveRelativeX, SIGNAL(returnPressed()), this, SLOT(MoveRelative()));
    connect(ui->moveRelativeY, SIGNAL(returnPressed()), this, SLOT(MoveRelative()));
    connect(ui->moveToChipButton, SIGNAL(clicked()), this, SLOT(MoveToChip()));
    connect(ui->moveToChipBox, SIGNAL(returnPressed()), this, SLOT(MoveToChip()));
    connect(ui->clearInputHistoryButton, SIGNAL(clicked()), this, SLOT(ClearInputHistory()));
    connect(ui->clearOutputHistoryButton, SIGNAL(clicked()), this, SLOT(ClearOutputHistory()));
    connect(ui->refreshLocationButton, SIGNAL(clicked()), this, SLOT(GetCurrentLocation()));
    connect(ui->moveHomeButton, SIGNAL(clicked()), this, SLOT(MoveHome()));
    connect(ui->setHomeButton, SIGNAL(clicked()), this, SLOT(SetHome()));
    connect(ui->resetIncrementButton, SIGNAL(clicked()), this, SLOT(ResetIncrement()));
    connect(ui->setContactButton, SIGNAL(clicked()), this, SLOT(SetContact()));
    connect(ui->setSeparateButton, SIGNAL(clicked()), this, SLOT(SetSeparate()));

    connect(serialPort, SIGNAL(readyRead()), this, SLOT(ReadProber()));

    connect(ui->clearMsgBox, SIGNAL(clicked()), this, SLOT(ClearMessages()));
    connect(ui->connectProberButton, SIGNAL(clicked()), this, SLOT(ToggleProberConnection()));
    connect(ui->setConnectionButton, SIGNAL(clicked()), this, SLOT(SetConnectionProperties()));
    connect(ui->readBufferButton, SIGNAL(clicked()), this, SLOT(ReadTheBuffer()));
    connect(ui->peekButton, SIGNAL(clicked()), this, SLOT(PeekAtBuffer()));

    connect(ui->positionReportingOffButton, SIGNAL(clicked()), this, SLOT(TurnOffPositionReporting()));
    connect(ui->setTVeloButton, SIGNAL(clicked()), this, SLOT(SetTVelocity()));
    connect(ui->setXVeloButton, SIGNAL(clicked()), this, SLOT(SetXVelocity()));
    connect(ui->setYVeloButton, SIGNAL(clicked()), this, SLOT(SetYVelocity()));
    connect(ui->initializeButton, SIGNAL(clicked()), this, SLOT(SendInitializeCommand()));
    connect(ui->setUnitsButton, SIGNAL(clicked()), this, SLOT(SetUnits()));
    connect(ui->setWaferSizeButton, SIGNAL(clicked()), this, SLOT(SetWaferSize()));

    connect(ui->alignButton1, SIGNAL(clicked()), this, SLOT(Align1()));

    connect(ui->alignButton2, SIGNAL(clicked()), this, SLOT(Align2()));
    connect(ui->alignMoveIncrementXBox, SIGNAL(returnPressed()), this, SLOT(Align2()));

    connect(ui->alignButton3, SIGNAL(clicked()), this, SLOT(Align3()));

    connect(ui->alignButton4, SIGNAL(clicked()), this, SLOT(Align4()));
    connect(ui->alignMoveRelativeXBox, SIGNAL(returnPressed()), this, SLOT(Align4()));
    connect(ui->alignMoveRelativeYBox, SIGNAL(returnPressed()), this, SLOT(Align4()));

    connect(ui->alignButton5, SIGNAL(clicked()), this, SLOT(Align5()));
    connect(ui->alignButton6, SIGNAL(clicked()), this, SLOT(Align6()));
    connect(ui->alignButton7, SIGNAL(clicked()), this, SLOT(Align7()));

    connect(ui->enableStartupToolsButton, SIGNAL(clicked()), this, SLOT(ToggleStartupTools()));


    serialPort->flush();

    SetStartupGUIStuff();
    SetConnectionGUIStuff();
    SetConnectionProperties();
//    ToggleProberConnection();
//    GetCurrentIncrement();
    return;
}

WP::~WP()
{
    if (serialPort->isOpen()) serialPort->close();
    delete serialPort;
    delete ui;
}
void WP::ToInputBox(QString txt)
{
    if (!txt.isEmpty()) ui->inputHistoryLog->appendPlainText(txt+"\n");
    return;
}
void WP::ToOutputBox(QString txt)
{
    if (!txt.isEmpty()) ui->outputHistoryLog->appendPlainText(txt);
    return;
}
void WP::SendCommand(QString cmd)
{
    if (!serialPort->isOpen())
    {
	Msg("serialPort not open!");
	return;
    }
    //Msg(QString("Writing command. cmd = %1").arg(cmd));
    ToggleInput(false);
    serialPort->write(cmd.toStdString().c_str(), cmd.length()+1);
    ToInputBox(cmd);
    return;
}
void WP::SendLineCommand()
{
    SendCommand(ui->commandLine->text());
    ui->commandLine->clear();
    return;
}
void WP::MoveZ()
{
    if (m_separated_flag)
    {
	SendCommand("MZ D C");
	ui->moveZButton->setText("Move to Separate");
    }
    else
    {
	SendCommand("MZ D S");
	ui->moveZButton->setText("Move to Contact");
    }
    m_separated_flag = !m_separated_flag;
    return;
}
void WP::MoveRelative()
{
    double dx = ui->moveRelativeX->text().toDouble();
    double dy = ui->moveRelativeY->text().toDouble();

    m_check_location_flag = true;								//Check the location after moving.

    QString cmd = QString("MR D X %1 Y %2").arg(dx).arg(dy);

    SendCommand(cmd);
    return;
}
void WP::MoveIncrement()
{
    m_check_location_flag = true;								//Check the location after moving.

    int dx = ui->moveIncrementX->text().toInt();
    int dy = ui->moveIncrementY->text().toInt();

    ui->moveIncrementX->clear();
    ui->moveIncrementY->clear();

    Msg(QString("dx = %1 dy = %2").arg(dx).arg(dy));
    //ui->moveIncrementX->setFocus();

    SendCommand(QString("MN D X %1 Y %2").arg(dx).arg(dy));
    return;
}
void WP::SetIncrement()
{
    m_check_location_flag = true;

    double dx = ui->setIncrementX->text().toDouble();
    double dy = ui->setIncrementY->text().toDouble();

    ui->setIncrementX->clear();                                                  //Clear the new value from the set increment boxes
    ui->setIncrementY->clear();

    m_chip_pitch_x = dx;
    m_chip_pitch_y = dy;

    QString cmd = QString("SN D X %1 Y %2").arg(dx).arg(dy);				//Concatenate the strings to form the command

    SendCommand(cmd);								    //Send the command
    return;
}
void WP::MoveToChip()
{
    int chip = ui->moveToChipBox->text().toInt();

    ui->moveToChipBox->clear();
    ui->moveToChipBox->setFocus();

    int col = 0, row = 0, currentcol = 0, currentrow = 0, xdist = 0, ydist = 0;

    currentcol = ui->currentColumnBox->text().toInt();
    currentrow = ui->currentRowBox->text().toInt();

    //Determine column and row of chip to move to.
    if ((chip > 60) || (chip < 1))
    {
	return;
    }
    else if ((chip <= 60) && (chip >= 56))
    {
	row = 60;
	col = 0;
    }
    else if ((chip <= 55) && (chip >= 49))
    {
	row = 54;
	col = 1;
    }
    else if ((chip <= 48) && (chip >= 40))
    {
	row = 46;
	col = 2;
    }
    else if ((chip <= 39) && (chip >= 31))
    {
	row = 37;
	col = 3;
    }
    else if ((chip <= 30) && (chip >= 22))
    {
	row = 28;
	col = 4;
    }
    else if ((chip <= 21) && (chip >= 13))
    {
	row = 19;
	col = 5;
    }
    else if ((chip <= 12) && (chip >= 6))
    {
	row = 11;
	col = 6;
    }
    else if ((chip <= 5) && (chip >= 1))
    {
	row = 5;
	col = 7;
    }
    else Msg("Error going to chip.");
    row -= chip;

    xdist = col-currentcol;
    ydist = row-currentrow;

    QString cmd = QString("MN D X %1 Y %2").arg(xdist).arg(ydist);
    m_check_location_flag = true;

    SendCommand(cmd);
    return;
}
void WP::ClearInputHistory()
{
    ui->inputHistoryLog->clear();
    return;
}
void WP::ClearOutputHistory()
{
    ui->outputHistoryLog->clear();
    return;
}
void WP::GetCurrentLocation()
{
    SendCommand("QP D");
    return;
}
void WP::MoveHome()
{
    m_check_location_flag = true;
    SendCommand("MH D");
    return;
}
void WP::SetHome()
{
    m_check_location_flag = true;
    SendCommand("SH D");
    return;
}
void WP::SetContact()
{
    QMessageBox QMB(this);
    QPushButton *QPB1 = QMB.addButton("Set Contact",QMessageBox::AcceptRole);
    QPushButton *QPB2 = QMB.addButton("Cancel",QMessageBox::RejectRole);
    QMB.setIcon(QMessageBox::Warning);
    QMB.setDefaultButton(QPB2);
    QMB.setText("Are you sure you want to set the contact position to the current Z position?");
    QMB.exec();

    if (QMB.clickedButton() == QPB2) return;
    else if (QMB.clickedButton() == QPB1) SendCommand("SZ D C");
    return;
}
void WP::SetSeparate()
{
    int new_separate_position = QInputDialog::getInt(this, "Set Separate Position", "Specify the desired distance from contact position, in �m.",700);
    if (new_separate_position<100)
    {
	QMessageBox QMB(this);
	QPushButton *QPB1 = QMB.addButton("I know what I'm doing",QMessageBox::AcceptRole);
	QPushButton *QPB2 = QMB.addButton("Reject changes",QMessageBox::RejectRole);
	QMB.setIcon(QMessageBox::Critical);
	QMB.setDefaultButton(QPB2);
	QMB.setText("It is highly recommended to set the separate position to more than 100.");
	QMB.setInformativeText(QString("At %1�m, you risk not pulling the needles off the DUT enough.").arg(new_separate_position));
	QMB.exec();

	if (QMB.clickedButton() == QPB2) return;
	else if (QMB.clickedButton() == QPB1) SendCommand(QString("SZ D S %1").arg(new_separate_position));
    }
    else if (new_separate_position>100 && new_separate_position<700)
    {
	QMessageBox QMB(this);
	QPushButton *QPB1 = QMB.addButton("Set Separate Position Anyway",QMessageBox::AcceptRole);
	QPushButton *QPB2 = QMB.addButton("Reject changes",QMessageBox::RejectRole);
	QMB.setIcon(QMessageBox::Warning);
	QMB.setDefaultButton(QPB2);
	QMB.setText("It is recommended to set the separate position for 700�m. You have it set for less.");
	QMB.setInformativeText(QString("700�m is safer than %1�m for DUT clearance.").arg(new_separate_position));
	QMB.exec();

	if (QMB.clickedButton() == QPB2) return;
	else if (QMB.clickedButton() == QPB1) SendCommand(QString("SZ D S %1").arg(new_separate_position));
    }
    else if (new_separate_position >= 700) SendCommand(QString("SZ D S %1").arg(new_separate_position));
    return;
}
void WP::ReadProber()
{
    ToggleInput(true);

    qint64 nBytes = serialPort->bytesAvailable();

    //Msg(QString("nBytes = %1").arg(nBytes));
    //qint64 nBytes = serialPort->bytesAvailable();

    int count = 0;
    top:
    QString response = "";
    Sleep(100);
    bool okflag = ReadBuffer(response, nBytes);
    if (!okflag || response.isEmpty())
    {
	//Msg(QString("okflag = %1\t response = %2").arg(okflag).arg(response));
	qApp->processEvents();
	count++;
	if (count != 0) goto top;					    //Obligatory goto statement
	return;
    }

    ToOutputBox(response);

    QStringList split_resp = response.split(" ");
    if (split_resp[0] == "QP")
    {
	m_check_location_flag = false;
	SetCurrentLocation(response);
    }
    else if (split_resp[0] == "QN") SetCurrentIncrement(response);

    if (m_check_location_flag) GetCurrentLocation();

    switch (alignmentlatch)
    {
    case 1:
	AlignRe1(response); //get first position->set the first position
	break;
    case 2:
	AlignRe2(response); //move relative->acknowledge movement
	break;
    case 3:
	AlignRe3(response); //get second position->set the second position
	break;
    case 4:
	AlignRe4(response); //move relative->wait for alignre5
	break;
    case 5:
	AlignRe5(response); //get third position->set the third position
	break;
    case 6:
	AlignRe6(response); //compute angle
	break;
    case 7:
	AlignRe7(response); //move angle
	break;
    default:
	if (alignmentlatch > 7) Msg(QString("Error: alignment latch should never be above 7. alignmentlatch = %1").arg(alignmentlatch));
    }
    return;

}
bool WP::ReadBuffer(QString &response, qint64 nBytes)
{
    QString txt = serialPort->peek(nBytes);
    //int ind = serialPort->peek(nBytes).indexOf('\n')+1;
    response = serialPort->readAll();
    return true;
//    Msg(QString("serialPort->peek(%2) = %1\nindex of '\\n' = %3").arg(txt).arg(nBytes).arg(ind));
//    if (ind == -1) return false;
//    else
//    {
//	response = serialPort->read(ind);
//	return true;
//    }
}

void WP::SetCurrentLocation(QString response) //Sets the current location.
{
    QString colbuf, rowbuf, chipbuf, xbuf, ybuf, tbuf;
    int Current_Col = 0, Current_Row = 0, Current_Chip = 0;
    double Current_X = 0, Current_Y = 0, Current_T = 0;

    if (!AnalyzePosition(response, Current_Col, Current_Row, Current_Chip, Current_X, Current_Y, Current_T))
    {
	ui->currentColumnBox->setText("err in AnalyzePosition");
	ui->currentRowBox->setText("err in AnalyzePosition");
	ui->currentChipBox->setText("err in AnalyzePosition");
	ui->currentXLocationBox->setText("err in AnalyzePosition");
	ui->currentYLocationBox->setText("err in AnalyzePosition");
	ui->currentTLocationBox->setText("err in AnalyzePosition");
	return;
    }

    if ((Current_Chip > 60) || (Current_Chip < 1)) chipbuf = "Outside of good chip region.";
    else chipbuf.setNum(Current_Chip);
    colbuf.setNum(Current_Col);						    //Convert to QString type.
    rowbuf.setNum(Current_Row);
    xbuf.setNum(Current_X);
    ybuf.setNum(Current_Y);
    tbuf.setNum(Current_T);
    ui->currentColumnBox->setText(colbuf);
    ui->currentRowBox->setText(rowbuf);
    ui->currentChipBox->setText(chipbuf);
    ui->currentXLocationBox->setText(xbuf);
    ui->currentYLocationBox->setText(ybuf);
    ui->currentTLocationBox->setText(tbuf);
    return;
}
bool WP::AnalyzePosition(QString response, int &col, int &row, int &chip, double &x, double &y, double &T)
{
    //response = "QP D X 20.3401 Y -19.3723 Z -0.0001 C -0.7000 T 0.0000";

    col = 0;						//Initialize to zero, just in case.
    row = 0;

    double xpos = 0, ypos = 0;				//Create doubles to so we can do math.

    //Split the response into the individual words.
    QStringList qsl = response.split(" "); //qsl = ["QP","D","X","20.3401","Y","-19.3723","Z","-0.0001","C","-0.7000","T","0.0000"]

    //If the response looks like it processed okay, go ahead and convert the stuff to double.
    if ((qsl[0] == "QP") && (qsl[1] == "D") && (qsl[2] == "X") && (qsl[4] == "Y") && (qsl[6] == "Z") &&(qsl[8] == "C") && (qsl[10] == "T"))
    {
	xpos = QString(qsl[3]).toDouble();
	ypos = QString(qsl[5]).toDouble();
	T = T ? 0 : QString(qsl[11]).toDouble();
    }
    else    //If the response is wrong, something went wrong.
    {
	m_check_location_flag = false;
	return false;
    }

    x = xpos;						//Set the current x and y positions.
    y = ypos;

    xpos = xpos/m_chip_pitch_x;
    ypos = ypos/m_chip_pitch_y;				//Divide by chip pitch to find the current column/row.

    if (abs(xpos-int(xpos))>=0.5)			//Round them off, because columns and rows are integers.
    {
	    if (xpos>=0) col = int(xpos+1);
	    else col = int(xpos-1);
    }
    else
    {
	    if (xpos>=0) col = int(xpos);
	    else col = int(xpos);
    };
    if (abs(ypos-int(ypos))>=0.5)
    {
	    if (ypos>=0) row = int(ypos+1);
	    else row = int(ypos-1);
    }
    else
    {
	    if (ypos>=0) row = int(ypos);
	    else row = int(ypos);
    };

    switch (col)					//Get the chip number using this cool method. I bet there's a faster way.
    {
    case 0:
	chip = 60;
	break;
    case 1:
	chip = 54;
	break;
    case 2:
	chip = 46;
	break;
    case 3:
	chip = 37;
	break;
    case 4:
	chip = 28;
	break;
    case 5:
	chip = 19;
	break;
    case 6:
	chip = 11;
	break;
    case 7:
	chip = 5;
	break;
    default:
	Msg(QString("Warning: Chip is outside of good region.col = %1").arg(col));
    }
    chip -= row;

    return true;
}
void WP::SetCurrentIncrement(QString response)
{
    QString xincr = "0", yincr = "0";

    if (!AnalyzeIncrement(response, xincr, yincr))
    {
	ui->currentXIncrementBox->setText("err in AnalyzeIncrement");
	ui->currentYIncrementBox->setText("err in AnalyzeIncrement");
	return;
    }

    ui->currentXIncrementBox->setText(xincr);
    ui->currentYIncrementBox->setText(yincr);
    return;
}
bool WP::AnalyzeIncrement(QString response, QString &xincr, QString &yincr)
{
    xincr = "0";
    yincr = "0";

    //QN D X 20.3202 Y 19.2625
    QStringList qsl = response.split(" ");

    if ((qsl[0] == "QN") && (qsl[1] == "D") && (qsl[2] == "X") && (qsl[4] == "Y")) //If the response is what we expect, return the x and y values.
    {
	xincr = QString(qsl[3]);
	yincr = QString(qsl[5]);
	return true;
    }
    else return false;
}
void WP::ClearMessages()
{
    ui->msgBox->clear();
    return;
}
void WP::Msg(QString txt)
{
    ui->msgBox->appendPlainText(txt);
    return;
}
void WP::ToggleInput(bool enflag)
{
    if (enflag)
    {
	ui->sendCommandButton->setEnabled(true);
	ui->moveZButton->setEnabled(true);
	ui->moveRelativeButton->setEnabled(true);
	ui->moveIncrementButton->setEnabled(true);
	ui->setIncrementButton->setEnabled(true);
	ui->moveToChipButton->setEnabled(true);
	ui->refreshLocationButton->setEnabled(true);
	ui->moveHomeButton->setEnabled(true);
	ui->setHomeButton->setEnabled(true);
	ui->resetIncrementButton->setEnabled(true);
	ui->setConnectionButton->setEnabled(true);

	ui->commandLine->setReadOnly(false);
	ui->moveRelativeX->setReadOnly(false);
	ui->moveRelativeY->setReadOnly(false);
	ui->moveIncrementX->setReadOnly(false);
	ui->moveIncrementY->setReadOnly(false);
	ui->setIncrementX->setReadOnly(false);
	ui->setIncrementY->setReadOnly(false);
	ui->moveToChipBox->setReadOnly(false);

	connect(ui->commandLine, SIGNAL(returnPressed()), this, SLOT(SendLineCommand()));
	connect(ui->moveRelativeX, SIGNAL(returnPressed()), this, SLOT(MoveRelative()));
	connect(ui->moveRelativeY, SIGNAL(returnPressed()), this, SLOT(MoveRelative()));
	connect(ui->moveIncrementX, SIGNAL(returnPressed()), this, SLOT(MoveIncrement()));
	connect(ui->moveIncrementY, SIGNAL(returnPressed()), this, SLOT(MoveIncrement()));
	connect(ui->setIncrementX, SIGNAL(returnPressed()), this, SLOT(SetIncrement()));
	connect(ui->setIncrementY, SIGNAL(returnPressed()), this, SLOT(SetIncrement()));
	connect(ui->moveToChipBox, SIGNAL(returnPressed()), this, SLOT(MoveToChip()));
    }
    else
    {
	ui->sendCommandButton->setEnabled(false);
	ui->moveZButton->setEnabled(false);
	ui->moveRelativeButton->setEnabled(false);
	ui->moveIncrementButton->setEnabled(false);
	ui->setIncrementButton->setEnabled(false);
	ui->moveToChipButton->setEnabled(false);
	ui->refreshLocationButton->setEnabled(false);
	ui->moveHomeButton->setEnabled(false);
	ui->setHomeButton->setEnabled(false);
	ui->resetIncrementButton->setEnabled(false);
	ui->setConnectionButton->setEnabled(false);

	ui->commandLine->setReadOnly(true);
	ui->moveRelativeX->setReadOnly(true);
	ui->moveRelativeY->setReadOnly(true);
	ui->moveIncrementX->setReadOnly(true);
	ui->moveIncrementY->setReadOnly(true);
	ui->setIncrementX->setReadOnly(true);
	ui->setIncrementY->setReadOnly(true);
	ui->moveToChipBox->setReadOnly(true);

	disconnect(ui->commandLine, SIGNAL(returnPressed()), this, SLOT(SendLineCommand()));
	disconnect(ui->moveRelativeX, SIGNAL(returnPressed()), this, SLOT(MoveRelative()));
	disconnect(ui->moveRelativeY, SIGNAL(returnPressed()), this, SLOT(MoveRelative()));
	disconnect(ui->moveIncrementX, SIGNAL(returnPressed()), this, SLOT(MoveIncrement()));
	disconnect(ui->moveIncrementY, SIGNAL(returnPressed()), this, SLOT(MoveIncrement()));
	disconnect(ui->setIncrementX, SIGNAL(returnPressed()), this, SLOT(SetIncrement()));
	disconnect(ui->setIncrementY, SIGNAL(returnPressed()), this, SLOT(SetIncrement()));
	disconnect(ui->moveToChipBox, SIGNAL(returnPressed()), this, SLOT(MoveToChip()));
    }
    return;
}
void WP::closeEvent(QCloseEvent *event)	//Ensures that the prober is in separate when it quits.
{
    if (!m_separated_flag)
    {
	int decision = ExitBox();
	if (decision == 1)
	{
	    SendCommand("MZ D S");
	    event->accept();
	}
	else if (decision == 0) event->accept();
	else if (decision == -1) event->ignore();
	return;
    }
}
int WP::ExitBox()				//Describes the pop up box which asks if you want to separate when you close the window.
{
    QMessageBox::StandardButton ret;
    ret = QMessageBox::warning(this, "Warning:","Move the prober to separate position before exiting?",
			 (QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel), QMessageBox::Yes);
    if (ret == QMessageBox::Yes) return 1;
    else if (ret == QMessageBox::No) return 0;
    else if (ret == QMessageBox::Cancel) return -1;
    else return 1;
}
void WP::SetConnectionProperties()
{
    SetProberBaudRate();
    SetProberDataBits();
    SetProberStopBits();
    SetProberParity();
    SetProberDeviceName();
    SetProberFlowControl();
    return;
}
void WP::ToggleProberConnection()
{
    if ((ui->connectProberButton->text() == "Connect") && (!serialPort->isOpen()))
    {
	m_check_location_flag = true;
	ConnectProber();
	Msg("Prober connected.");
	GetCurrentIncrement();
    }
    else if ((ui->connectProberButton->text() == "Disconnect") && (serialPort->isOpen()))
    {
	DisconnectProber();
	Msg("Prober disconnected.");
    }
    else
    {
	DisconnectProber();
	Msg("serialPort state is not synced with connectproberbutton state. serialPort and connectproberbutton are disconnected now.");
    }
    return;
}
void WP::ConnectProber()
{
    ui->connectProberButton->setText("Disconnect");
    serialPort->open(QIODevice::ReadWrite);
    return;
}
void WP::DisconnectProber()
{
    ui->connectProberButton->setText("Connect");
    serialPort->close();
    return;
}
void WP::SetProberBaudRate()
{
    BaudRateType num;
    int baudrate = ui->baudRateBox->currentText().toInt();
    switch(baudrate)
    {
    case 1200:
	num = BAUD1200;
	break;
    case 2400:
	num = BAUD2400;
	break;
    case 4800:
	num = BAUD4800;
	break;
    case 9600:
	num = BAUD9600;
	break;
    case 19200:
	num = BAUD19200;
	break;
    case 38400:
	num = BAUD38400;
	break;
    case 57600:
	num = BAUD57600;
	break;
    case 115200:
	num = BAUD115200;
	break;
    default:
	num = BAUD9600;
    }
    Msg(QString("Setting Baud Rate: %1").arg(baudrate));
    serialPort->setBaudRate(num);
    return;
}
void WP::SetProberFlowControl()
{
    QString text = ui->flowControlBox->currentText();
    FlowType fc;
    if (text == "FlowControlOff") fc = FLOW_OFF;
    else if (text == "FlowControlHardware") fc = FLOW_HARDWARE;
    else if (text == "FlowControlSoftware") fc = FLOW_XONXOFF;
    else fc = FLOW_OFF;
    Msg(QString("Setting Flow Control: %1").arg(text));
    serialPort->setFlowControl(fc);
    return;
}
void WP::SetProberStopBits()
{
    double db = ui->flowControlBox->currentText().toDouble();
    StopBitsType sb;
    if (db == 1) sb = STOP_1;
    else if (db == 1.5) sb = STOP_1_5;
    else if (db == 2) sb = STOP_2;
    else sb = STOP_1;					    //Default

    Msg(QString("Setting Stop Bits: %1").arg(db));
    serialPort->setStopBits(sb);
    return;
}
void WP::SetProberDataBits()
{
    DataBitsType db;
    int databits = ui->dataBitsBox->currentText().toInt();
    switch(databits)
    {
    case 5:
	db = DATA_5;
	break;
    case 6:
	db = DATA_6;
	break;
    case 7:
	db = DATA_7;
	break;
    case 8:
	db = DATA_8;
	break;
    default:						    //Default
	db = DATA_7;
    }
    Msg(QString("Setting Data Bits: %1").arg(databits));
    serialPort->setDataBits(db);
    return;
}
void WP::SetProberParity()
{
    QString text = ui->parityBox->currentText();

    ParityType pa;
    if (text == "None") pa = PAR_NONE;
    else if (text == "Odd") pa = PAR_ODD;
    else if (text == "Even") pa = PAR_EVEN;
    else if (text == "Mark") pa = PAR_MARK;
    else if (text == "Space") pa = PAR_SPACE;
    else pa = PAR_NONE;				//default

    Msg(QString("Setting Parity: %1").arg(text));
    serialPort->setParity(pa);

    return;
}
void WP::SetProberDeviceName()
{
    QString name = ui->deviceNameBox->text();
    Msg(QString("Setting Device Name: %1").arg(name));
    serialPort->setPortName(name);
    return;
}
void WP::SetConnectionGUIStuff()
{
    QStringList BaudRateBoxList;
    BaudRateBoxList = QString("1200,2400,4800,9600,19200,38400,57600,115200").split(","); //CAN ADD MORE HERE!
    ui->baudRateBox->addItems(BaudRateBoxList);
    ui->baudRateBox->setCurrentIndex(3);

    QStringList DataBitsBoxList;
    DataBitsBoxList = QString("5,6,7,8").split(",");
    ui->dataBitsBox->addItems(DataBitsBoxList);
    ui->dataBitsBox->setCurrentIndex(2);

    QStringList ParityBoxList;
    ParityBoxList = QString("None,Odd,Even,Mark,Space").split(",");
    ui->parityBox->addItems(ParityBoxList);
    ui->parityBox->setCurrentIndex(2);

    ui->deviceNameBox->setText("COM1");

    QStringList FlowControlBoxList;
    FlowControlBoxList = QString("FlowControlOff,FlowControlHardware,FlowControlSoftware").split(",");
    ui->flowControlBox->addItems(FlowControlBoxList);
    ui->flowControlBox->setCurrentIndex(0);

    QStringList StopBitsBoxList;
    //StopBitsBoxList = QString("StopBits1,StopBits1.5,StopBits2").split(",");
    StopBitsBoxList = QString("1,1.5,2").split(",");
    ui->stopBitsBox->addItems(StopBitsBoxList);
    ui->stopBitsBox->setCurrentIndex(0);
    return;
}
void WP::GetCurrentIncrement()
{
    m_check_location_flag = true;
    SendCommand("QN D");
    return;
}
void WP::ResetIncrement()
{
    SendCommand(QString("SN D X %1 Y %2").arg(m_chip_pitch_x).arg(m_chip_pitch_y));
    return;
}
void WP::PeekAtBuffer()
{
    ToOutputBox(QString(serialPort->peek(serialPort->bytesAvailable())));
    return;
}
void WP::ReadTheBuffer()
{
    ToOutputBox(QString(serialPort->readAll()));
    return;
}
void WP::TurnOffPositionReporting()
{
    SendCommand("SS M B POSI D");
    return;
}
void WP::SetTVelocity()
{
    SendCommand(QString("SM D T VELO %1").arg(ui->tVeloBox->value()));
    return;
}
void WP::SetXVelocity()
{
    SendCommand(QString("SM D X VELO %1").arg(ui->xVeloBox->value()));
    return;
}
void WP::SetYVelocity()
{
    SendCommand(QString("SM D Y VELO %1").arg(ui->yVeloBox->value()));
    return;
}
void WP::SetWaferSize()
{
    SendCommand(QString("SS C WAFE %1").arg(ui->waferSizeBox->value()));
    return;
}
void WP::SendInitializeCommand()
{
    SendCommand("MM D 0 INIT");
    return;
}
void WP::SetUnits()
{
    QString units;
    QString current_text = ui->unitsBox->currentText();
    if (current_text == "English")
    {
	units = "E";
	ui->setTVeloButton->setText("Set T Velocity (deg/s)");
	ui->setXVeloButton->setText("Set X Velocity (in/s)");
	ui->setYVeloButton->setText("Set Y Velocity (in/s)");
	ui->setWaferSizeButton->setText("Set Wafer Size (in)");
    }
    else if (current_text == "Metric")
    {
	units = "M";
	ui->setTVeloButton->setText("Set T Velocity (mm/s)");
	ui->setXVeloButton->setText("Set X Velocity (mm/s)");
	ui->setYVeloButton->setText("Set Y Velocity (mm/s)");
	ui->setWaferSizeButton->setText("Set Wafer Size (mm)");
    }
    else return;
    SendCommand(QString("SS C UNIT %1").arg(units));
    return;
}
void WP::SetStartupGUIStuff()
{
    QStringList UnitsBoxList = QString("Metric,English").split(",");
    ui->unitsBox->addItems(UnitsBoxList);
    return;
}
void WP::Align1()
{
    //Msg("Align1()");
    //Msg(QString("Alignmentlatch = %1").arg(alignmentlatch));
    ui->alignmentLabel->setText("Getting first location.");

    alignmentlatch = 1;
    SendCommand("QP D");
    ui->alignButton1->setEnabled(false);	//Disable button 1. Enable button 2 when response comes.
    return;
}
void WP::Align2()
{
    alignmentlatch = 2;
    //Msg("Align2()");
    //Msg(QString("Alignmentlatch = %1").arg(alignmentlatch));
    ui->alignmentLabel->setText("Moving increment.");

    QString dx = ui->alignMoveIncrementXBox->text();
    ui->alignMoveIncrementXBox->setReadOnly(true);
    ui->alignButton2->setEnabled(false);	//Disable button 2. Enable button 3 when response comes.

    SendCommand(QString("MN D X %1").arg(dx));
    return;
}
void WP::Align3()
{
    alignmentlatch = 3;
    //Msg("Align3()");
    //Msg(QString("Alignmentlatch = %1").arg(alignmentlatch));
    ui->alignmentLabel->setText("Getting second location.");

    SendCommand("QP D");
    ui->alignButton3->setEnabled(false);	//Disable button 3. Enable buttons 4 and 5 when response comes.
    return;
}
void WP::Align4()
{
    alignmentlatch = 4;
    //Msg("Align4()");
    //Msg(QString("Alignmentlatch = %1").arg(alignmentlatch));
    ui->alignmentLabel->setText("Moving relative.");

    double dx = ui->alignMoveRelativeXBox->text().toDouble();
    double dy = ui->alignMoveRelativeYBox->text().toDouble();
    SendCommand(QString("MR D X %1 Y %2").arg(dx).arg(dy));
    return;
}
void WP::Align5()
{
    alignmentlatch = 5;
    //Msg("Align5()");
    //Msg(QString("Alignmentlatch = %1").arg(alignmentlatch));
    ui->alignmentLabel->setText("Getting third location.");

    SendCommand("QP D");

    ui->alignMoveRelativeXBox->setReadOnly(true);
    ui->alignMoveRelativeYBox->setReadOnly(true);
    ui->alignButton4->setEnabled(false);
    ui->alignButton5->setEnabled(false);
    return;
}
void WP::Align6()
{
    alignmentlatch = 6;
    //Msg("Align6()");
    //Msg(QString("Alignmentlatch = %1").arg(alignmentlatch));
    ui->alignmentLabel->setText("Calculating change in angle.");

    ui->alignButton6->setEnabled(false);
    AlignRe6("");
    return;
}
void WP::Align7()
{
    alignmentlatch = 7;
    //Msg("Align7()");
    //Msg(QString("Alignmentlatch = %1").arg(alignmentlatch));
    ui->alignmentLabel->setText("Rotating to align.");

    SendCommand(QString("MR D T %1").arg(ui->alignComputeRotationBox->text().toDouble()));
    ui->alignButton7->setEnabled(false);
    return;
}
void WP::AlignRe1(QString response)
{
    //Msg("Align1()");
    //Msg(QString("Alignmentlatch = %1").arg(alignmentlatch));
    QString col = ui->currentColumnBox->text();
    QString row = ui->currentRowBox->text();
    QString chip = ui->currentChipBox->text();
    QString xloc = ui->currentXLocationBox->text();
    QString yloc = ui->currentYLocationBox->text();
    QString tloc = ui->currentTLocationBox->text();

    QStringList qsl;
    qsl += col;
    qsl += row;
    qsl += chip;
    qsl += xloc;
    qsl += yloc;
    qsl += tloc;

    if (qsl.contains("err in AnalyzePosition"))
    {
       ui->alignmentLabel->setText("error in AnalyzePosition.\nAlignment aborted. Returning.");
       alignmentlatch = 0;
       return;
    }
    else
    {
	ui->alignLocationTBox1->setText(tloc);
	ui->alignLocationXBox1->setText(xloc);
	ui->alignLocationYBox1->setText(yloc);
	ui->alignButton2->setEnabled(true);	    //Enable button 2.
	ui->alignmentLabel->setText("Move incrementally to\nother side of wafer.");
    }
    return;
}
void WP::AlignRe2(QString response)
{
    //Msg("Align2()");
    //Msg(QString("Alignmentlatch = %1").arg(alignmentlatch));
    QStringList qsl = response.split(" ");
    if ((qsl[0] == "RR") && (qsl[1] == "MN")) ui->alignButton3->setEnabled(true);	//Enable button 3.
    else
    {
	Msg("Alignment Error.");
	return;
    }
    ui->alignmentLabel->setText("Now get the second location.");
    return;
}
void WP::AlignRe3(QString response)
{
    //Msg("Align3()");
    //Msg(QString("Alignmentlatch = %1").arg(alignmentlatch));
    QString col = ui->currentColumnBox->text();
    QString row = ui->currentRowBox->text();
    QString chip = ui->currentChipBox->text();
    QString xloc = ui->currentXLocationBox->text();
    QString yloc = ui->currentYLocationBox->text();
    QString tloc = ui->currentTLocationBox->text();

    QStringList qsl;
    qsl += col;
    qsl += row;
    qsl += chip;
    qsl += xloc;
    qsl += yloc;
    qsl += tloc;

    if (qsl.contains("err in AnalyzePosition"))
    {
	ui->alignmentLabel->setText("error in AnalyzePosition.\nAlignment aborted. Returning.");
	alignmentlatch = 0;
	return;
    }
    else
    {
	ui->alignLocationTBox2->setText(tloc);
	ui->alignLocationXBox2->setText(xloc);
	ui->alignLocationYBox2->setText(yloc);
	ui->alignButton4->setEnabled(true);		//Enable buttons 4 and 5.
	ui->alignButton5->setEnabled(true);
	ui->alignmentLabel->setText("Use the move relative command to center the\nneedle on the pad. When you are done,\nget the current position.");
    }
    return;
}
void WP::AlignRe4(QString response)
{
    //Msg("Align4()");
    //Msg(QString("Alignmentlatch = %1").arg(alignmentlatch));
    QStringList qsl = response.split(" ");
    if (!((qsl[0] == "RR") && (qsl[1] == "MR")))	//until button 5 is presssed.
    {
	Msg("Alignment Error.");
	return;
    }
    else ui->alignmentLabel->setText("Use the move relative command to center the\nneedle on the pad. When you are done,\nget the current position.");

    return;
}
void WP::AlignRe5(QString response)
{
    //Msg("Align5()");
    //Msg(QString("Alignmentlatch = %1").arg(alignmentlatch));
    QString col = ui->currentColumnBox->text();
    QString row = ui->currentRowBox->text();
    QString chip = ui->currentChipBox->text();
    QString xloc = ui->currentXLocationBox->text();
    QString yloc = ui->currentYLocationBox->text();
    QString tloc = ui->currentTLocationBox->text();

    QStringList qsl;
    qsl += col;
    qsl += row;
    qsl += chip;
    qsl += xloc;
    qsl += yloc;
    qsl += tloc;

    if (qsl.contains("err in AnalyzePosition"))
    {
	ui->alignmentLabel->setText("error in AnalyzePosition.\nAlignment aborted. Returning.");
	alignmentlatch = 0;
	return;
    }
    else
    {
	ui->alignLocationTBox3->setText(tloc);
	ui->alignLocationXBox3->setText(xloc);
	ui->alignLocationYBox3->setText(yloc);
	ui->alignButton6->setEnabled(true);
	ui->alignmentLabel->setText("Compute the amount to move.");
    }
    return;
}
void WP::AlignRe6(QString response)
{
    //Msg("Align6()");
    //Msg(QString("Alignmentlatch = %1").arg(alignmentlatch));
    double dx = 0, dy = 0, dt = 0;
    dx = ui->alignLocationXBox2->text().toDouble() - ui->alignLocationXBox1->text().toDouble();
    dy = ui->alignLocationYBox3->text().toDouble() - ui->alignLocationYBox2->text().toDouble();
    dt = -atan(dy/dx)*(180.0/PI);		//Convert to degrees, which the station uses.
    ui->alignComputeRotationBox->setText(QString("%1").arg(dt));
    ui->alignButton7->setEnabled(true);
    ui->alignmentLabel->setText("Click Rotate DUT to align.");
    return;
}
void WP::AlignRe7(QString response)
{
    //Msg("Align7()");
    //Msg(QString("Alignmentlatch = %1").arg(alignmentlatch));
    QStringList qsl = response.split(" ");
    if ((qsl[0] == "RR") && (qsl[1] == "MR"))
    {
	Msg("Alignment Complete");
	ui->alignmentLabel->setText("Done!");
    }
    else
    {
	Msg("Alignment Error.");
	return;
    }
    alignmentlatch = 0;
    ui->alignButton1->setEnabled(true);
    return;
}
void WP::ToggleStartupTools()
{
    if (ui->startupToolsGroup->isEnabled())
    {
	ui->enableStartupToolsButton->setText("Enable Startup Configuration Tools");
	ui->startupToolsGroup->setEnabled(false);
    }
    else
    {
	QMessageBox::StandardButton ret;
	ret = QMessageBox::critical(this, "Warning:","DON'T do this willy-nilly. If you're sure you want to do this, make sure the DUT is out of the way. "
				    "Be advised that the Send Initialization Command resets all your settings, and ignores your contact and separate positions, "
				    "so you risk utter annihilation of your DUT if you choose to do this.",
			     (QMessageBox::Yes | QMessageBox::No), QMessageBox::No);
	if (ret == QMessageBox::No) return;
	else if (ret == QMessageBox::Yes)
	{
	    ui->startupToolsGroup->setEnabled(true);
	    ui->enableStartupToolsButton->setText("Disable Startup Configuration Tools");
	}
    }
    return;
}
