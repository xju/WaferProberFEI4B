#-------------------------------------------------
#
# Project created by QtCreator 2012-05-24T10:50:49
#
#-------------------------------------------------

QT       += core gui
QT	+= qt3support

TARGET = WaferProber
TEMPLATE = app


SOURCES += main.cxx\
        wp.cxx

HEADERS  += wp.h

FORMS    += wp.ui

include(qextserialport/src/qextserialport.pri)
