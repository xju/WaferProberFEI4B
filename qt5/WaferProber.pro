QT += core gui
QT += widgets

TARGET = WaferProberQt5p9
TEMPLATE = app

FORMS += \
    wp.ui

HEADERS += \
    wp.h

SOURCES += \
    wp.cxx \
    main.cxx

include(qextserialport/src/qextserialport.pri)
