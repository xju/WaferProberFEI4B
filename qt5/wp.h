#ifndef WP_H
#define WP_H

#include <cstdlib>
// #include "Windows.h"

#include <QMainWindow>
#include <QMessageBox>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QTreeView>
#include <QHeaderView>
#include <QInputDialog>
#include <QComboBox>
#include <QTimer>
#include <string>
#include <cmath> //If you don't include this, abs() returns an int, not a double or float!!!!!!!
//#include "H:/qserialdevice-qserialdevice/include/serialport.h"
//#include "H:/qserialdevice-qserialdevice/include/serialportinfo.h"
#include "qextserialport/src/qextserialport.h"


namespace Ui {
class WP;
}

class WP : public QMainWindow
{
    Q_OBJECT

protected:
    void closeEvent(QCloseEvent *);
    
public:
    explicit WP(QWidget *parent = 0);
    ~WP();

    QextSerialPort *serialPort;
    unsigned short int alignmentlatch;
    bool m_separated_flag, m_check_location_flag;
    double m_chip_pitch_x, m_chip_pitch_y, PI;

    bool AnalyzePosition(QString, int&, int&, int&, double&, double&, double&);	//Called by SetCurrentPosition to get the numbers out of the response, and does math on 'em.
    bool AnalyzeIncrement(QString, QString&, QString&);	    //Called by SetCurrentIncrement to get the numbers out of the response.
    void GetCurrentIncrement();		    //Call this to get the current increment, and update the current location.
    void SetCurrentLocation(QString);	    //Sets the displayed current location from the response, which is the parameter.
    void SetCurrentIncrement(QString);	    //Sets the displayed current increment from the response, which is the parameter.
    int ExitBox();			    //If the prober is in contact and you try to exit, this creates a popup warning.
    void ToggleInput(bool);		    //Disables and enables input, so that when the prober is doing something you can't tell it to do something else.

    void SendCommand(QString);		    //Used to send a command.
    bool ReadBuffer(QString &response, qint64 nBytes);	    //Reads/cleans out the buffer.

    void SetProberDataBits();
    void SetProberStopBits();
    void SetProberParity();
    void SetProberDeviceName();
    void SetProberFlowControl();
    void SetProberBaudRate();

    void ConnectProber();
    void DisconnectProber();

    void ToInputBox(QString);
    void ToOutputBox(QString);
    void Msg(QString);

    void SetConnectionGUIStuff();
    void SetStartupGUIStuff();

    void AlignRe1(QString);
    void AlignRe2(QString);
    void AlignRe3(QString);
    void AlignRe4(QString);
    void AlignRe5(QString);
    void AlignRe6(QString);
    void AlignRe7(QString);

public slots:
    void MoveZ();			    //Uses separatedflag to alternately send contact and separate messages when clicking movez button.
    void SetIncrement();		    //Function which is called when clicking setIncrementButton
    void MoveIncrement();		    // "	    "			"   moveIncrementButton
    void MoveRelative();		    //			"		    moveRelativeButton

    void SendLineCommand();		    //Called when sendCommandButton is clicked. Calls SendCommand function.
    void MoveToChip();			    //Moves to any chip you want by calculating the distance to move.
    void ClearInputHistory();		    //Clears the input history.
    void ClearOutputHistory();		    //Clears the output history.
    void GetCurrentLocation();		    //Call this to get the current location.
    void MoveHome();			    //Moves to the home position.
    void SetHome();			    //Sets the home position.
    void ResetIncrement();		    //Resets increment to  FE-I4 defaults.
    void SetContact();			    //Sets the contact position to the current prober position
    void SetSeparate();			    //Pops up a dialog box which allows you to set the separate position, relative to current contact position.

    void ReadProber();

    void SetConnectionProperties();		    //Sets the baud rate, parity, stop bits, data bits, com port, and handshake of the connection
    void ToggleProberConnection();		    //Connects and disconnects to the prober

    void PeekAtBuffer();
    void ReadTheBuffer();

    void ClearMessages();

    void SetXVelocity();
    void SetYVelocity();
    void SetTVelocity();
    void SendInitializeCommand();
    void TurnOffPositionReporting();
    void SetWaferSize();
    void SetUnits();

    void Align1();
    void Align2();
    void Align3();
    void Align4();
    void Align5();
    void Align6();
    void Align7();

    void ToggleStartupTools();
    
private:
    Ui::WP *ui;
};

#endif // WP_H
