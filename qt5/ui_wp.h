/********************************************************************************
** Form generated from reading UI file 'wp.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WP_H
#define UI_WP_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WP
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_8;
    QHBoxLayout *horizontalLayout_13;
    QVBoxLayout *verticalLayout_4;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QLabel *inputHistoryLabel;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_3;
    QLabel *probeStationOutputLabel;
    QSpacerItem *horizontalSpacer_4;
    QPlainTextEdit *inputHistoryLog;
    QHBoxLayout *horizontalLayout_3;
    QLabel *enterCommandLabel;
    QLineEdit *commandLine;
    QPushButton *sendCommandButton;
    QPlainTextEdit *outputHistoryLog;
    QGridLayout *gridLayout_2;
    QSpacerItem *horizontalSpacer_9;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_5;
    QLabel *XAxisLabel;
    QSpacerItem *horizontalSpacer_6;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_7;
    QLabel *YAxisLabel;
    QSpacerItem *horizontalSpacer_8;
    QHBoxLayout *horizontalLayout_12;
    QSpacerItem *horizontalSpacer_20;
    QLabel *label_18;
    QSpacerItem *horizontalSpacer_21;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_10;
    QLabel *ZAxisLabel;
    QSpacerItem *horizontalSpacer_11;
    QPushButton *moveRelativeButton;
    QLineEdit *moveRelativeX;
    QLineEdit *moveRelativeY;
    QHBoxLayout *horizontalLayout_8;
    QSpacerItem *horizontalSpacer_14;
    QPushButton *moveZButton;
    QSpacerItem *horizontalSpacer_15;
    QPushButton *moveIncrementButton;
    QLineEdit *moveIncrementX;
    QLineEdit *moveIncrementY;
    QGroupBox *otherMovementFunctionsBox;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_16;
    QVBoxLayout *verticalLayout_5;
    QPushButton *moveHomeButton;
    QPushButton *setHomeButton;
    QPushButton *setContactButton;
    QPushButton *setSeparateButton;
    QPushButton *setIncrementButton;
    QLineEdit *setIncrementX;
    QLineEdit *setIncrementY;
    QLabel *currentIncrementLabel;
    QLineEdit *currentXIncrementBox;
    QLineEdit *currentYIncrementBox;
    QLabel *currentLocationLabel;
    QLineEdit *currentXLocationBox;
    QLineEdit *currentYLocationBox;
    QHBoxLayout *horizontalLayout_11;
    QSpacerItem *horizontalSpacer_13;
    QLabel *columnLabel;
    QLineEdit *currentColumnBox;
    QHBoxLayout *horizontalLayout_7;
    QSpacerItem *horizontalSpacer_12;
    QLabel *rowLabel;
    QLineEdit *currentRowBox;
    QLabel *currentChipLabel;
    QLineEdit *currentChipBox;
    QPushButton *moveToChipButton;
    QLineEdit *moveToChipBox;
    QLineEdit *currentTLocationBox;
    QTabWidget *tabWidget;
    QWidget *proberInfoTab;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_17;
    QSpacerItem *horizontalSpacer_28;
    QPushButton *connectProberButton;
    QSpacerItem *horizontalSpacer_29;
    QGridLayout *gridLayout_7;
    QLabel *label_3;
    QComboBox *baudRateBox;
    QLabel *label_13;
    QComboBox *parityBox;
    QLabel *label_14;
    QLineEdit *deviceNameBox;
    QLabel *label_15;
    QComboBox *stopBitsBox;
    QLabel *label_16;
    QComboBox *dataBitsBox;
    QLabel *label_17;
    QComboBox *flowControlBox;
    QHBoxLayout *horizontalLayout_20;
    QSpacerItem *horizontalSpacer_26;
    QPushButton *setConnectionButton;
    QSpacerItem *horizontalSpacer_27;
    QHBoxLayout *horizontalLayout_22;
    QSpacerItem *horizontalSpacer_32;
    QPushButton *peekButton;
    QPushButton *readBufferButton;
    QSpacerItem *horizontalSpacer_33;
    QSpacerItem *verticalSpacer_14;
    QWidget *otherFunctionsTab;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_9;
    QPushButton *clearOutputHistoryButton;
    QPushButton *clearInputHistoryButton;
    QPushButton *clearMsgBox;
    QHBoxLayout *horizontalLayout_10;
    QSpacerItem *horizontalSpacer_16;
    QPushButton *refreshLocationButton;
    QSpacerItem *horizontalSpacer_18;
    QHBoxLayout *horizontalLayout_15;
    QSpacerItem *horizontalSpacer_24;
    QPushButton *resetIncrementButton;
    QSpacerItem *horizontalSpacer_25;
    QHBoxLayout *horizontalLayout_21;
    QSpacerItem *horizontalSpacer_30;
    QPushButton *enableStartupToolsButton;
    QSpacerItem *horizontalSpacer_31;
    QGroupBox *startupToolsGroup;
    QGridLayout *gridLayout_10;
    QGridLayout *gridLayout_4;
    QPushButton *positionReportingOffButton;
    QPushButton *setTVeloButton;
    QPushButton *setXVeloButton;
    QPushButton *setYVeloButton;
    QPushButton *initializeButton;
    QPushButton *setUnitsButton;
    QComboBox *unitsBox;
    QPushButton *setWaferSizeButton;
    QDoubleSpinBox *tVeloBox;
    QDoubleSpinBox *xVeloBox;
    QDoubleSpinBox *yVeloBox;
    QDoubleSpinBox *waferSizeBox;
    QSpacerItem *verticalSpacer;
    QWidget *waferTab;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout_2;
    QGridLayout *gridLayout_3;
    QPushButton *alignButton1;
    QGridLayout *gridLayout_8;
    QLabel *label_7;
    QLineEdit *alignLocationXBox1;
    QLabel *label_8;
    QLineEdit *alignLocationYBox1;
    QLabel *label_9;
    QLineEdit *alignLocationTBox1;
    QSpacerItem *verticalSpacer_3;
    QSpacerItem *verticalSpacer_4;
    QHBoxLayout *horizontalLayout_18;
    QPushButton *alignButton2;
    QLabel *label;
    QLineEdit *alignMoveIncrementXBox;
    QSpacerItem *verticalSpacer_5;
    QGridLayout *gridLayout_16;
    QPushButton *alignButton3;
    QGridLayout *gridLayout_5;
    QLabel *label_4;
    QLineEdit *alignLocationXBox2;
    QLabel *label_5;
    QLineEdit *alignLocationYBox2;
    QLabel *label_6;
    QLineEdit *alignLocationTBox2;
    QSpacerItem *verticalSpacer_6;
    QSpacerItem *verticalSpacer_9;
    QGridLayout *gridLayout_18;
    QPushButton *alignButton4;
    QGridLayout *gridLayout_17;
    QLabel *label_2;
    QLineEdit *alignMoveRelativeXBox;
    QLabel *label_23;
    QLineEdit *alignMoveRelativeYBox;
    QSpacerItem *verticalSpacer_7;
    QSpacerItem *verticalSpacer_10;
    QGridLayout *gridLayout_19;
    QPushButton *alignButton5;
    QGridLayout *gridLayout_9;
    QLabel *label_10;
    QLineEdit *alignLocationXBox3;
    QLabel *label_11;
    QLineEdit *alignLocationYBox3;
    QLabel *label_12;
    QLineEdit *alignLocationTBox3;
    QSpacerItem *verticalSpacer_8;
    QSpacerItem *verticalSpacer_11;
    QHBoxLayout *horizontalLayout_19;
    QPushButton *alignButton6;
    QLineEdit *alignComputeRotationBox;
    QPushButton *alignButton7;
    QSpacerItem *verticalSpacer_12;
    QHBoxLayout *horizontalLayout_14;
    QSpacerItem *horizontalSpacer_22;
    QLabel *alignmentLabel;
    QSpacerItem *horizontalSpacer_23;
    QSpacerItem *verticalSpacer_13;
    QWidget *libraryTab;
    QGridLayout *gridLayout_6;
    QTreeWidget *treeWidget;
    QPlainTextEdit *msgBox;
    QMenuBar *menuBar;
    QMenu *menuVersion_5_0;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *WP)
    {
        if (WP->objectName().isEmpty())
            WP->setObjectName(QStringLiteral("WP"));
        WP->setEnabled(true);
        WP->resize(1094, 900);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(WP->sizePolicy().hasHeightForWidth());
        WP->setSizePolicy(sizePolicy);
        centralWidget = new QWidget(WP);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout_8 = new QVBoxLayout(centralWidget);
        verticalLayout_8->setSpacing(6);
        verticalLayout_8->setContentsMargins(11, 11, 11, 11);
        verticalLayout_8->setObjectName(QStringLiteral("verticalLayout_8"));
        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setSpacing(6);
        horizontalLayout_13->setObjectName(QStringLiteral("horizontalLayout_13"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        inputHistoryLabel = new QLabel(centralWidget);
        inputHistoryLabel->setObjectName(QStringLiteral("inputHistoryLabel"));

        horizontalLayout_2->addWidget(inputHistoryLabel);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        gridLayout->addLayout(horizontalLayout_2, 0, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);

        probeStationOutputLabel = new QLabel(centralWidget);
        probeStationOutputLabel->setObjectName(QStringLiteral("probeStationOutputLabel"));

        horizontalLayout->addWidget(probeStationOutputLabel);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);


        gridLayout->addLayout(horizontalLayout, 0, 1, 1, 1);

        inputHistoryLog = new QPlainTextEdit(centralWidget);
        inputHistoryLog->setObjectName(QStringLiteral("inputHistoryLog"));
        inputHistoryLog->setMinimumSize(QSize(295, 201));
        inputHistoryLog->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        gridLayout->addWidget(inputHistoryLog, 1, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        enterCommandLabel = new QLabel(centralWidget);
        enterCommandLabel->setObjectName(QStringLiteral("enterCommandLabel"));

        horizontalLayout_3->addWidget(enterCommandLabel);

        commandLine = new QLineEdit(centralWidget);
        commandLine->setObjectName(QStringLiteral("commandLine"));

        horizontalLayout_3->addWidget(commandLine);

        sendCommandButton = new QPushButton(centralWidget);
        sendCommandButton->setObjectName(QStringLiteral("sendCommandButton"));
        sendCommandButton->setMinimumSize(QSize(82, 23));
        sendCommandButton->setMaximumSize(QSize(82, 23));
        sendCommandButton->setAutoDefault(false);

        horizontalLayout_3->addWidget(sendCommandButton);


        gridLayout->addLayout(horizontalLayout_3, 2, 0, 1, 2);

        outputHistoryLog = new QPlainTextEdit(centralWidget);
        outputHistoryLog->setObjectName(QStringLiteral("outputHistoryLog"));
        outputHistoryLog->setMinimumSize(QSize(294, 201));
        outputHistoryLog->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        gridLayout->addWidget(outputHistoryLog, 1, 1, 1, 1);


        verticalLayout_4->addLayout(gridLayout);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        horizontalSpacer_9 = new QSpacerItem(88, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_9, 0, 0, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_5);

        XAxisLabel = new QLabel(centralWidget);
        XAxisLabel->setObjectName(QStringLiteral("XAxisLabel"));

        horizontalLayout_4->addWidget(XAxisLabel);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_6);


        gridLayout_2->addLayout(horizontalLayout_4, 0, 1, 1, 1);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalSpacer_7 = new QSpacerItem(40, 18, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_7);

        YAxisLabel = new QLabel(centralWidget);
        YAxisLabel->setObjectName(QStringLiteral("YAxisLabel"));

        horizontalLayout_5->addWidget(YAxisLabel);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_8);


        gridLayout_2->addLayout(horizontalLayout_5, 0, 2, 1, 1);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setSpacing(6);
        horizontalLayout_12->setObjectName(QStringLiteral("horizontalLayout_12"));
        horizontalSpacer_20 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_12->addItem(horizontalSpacer_20);

        label_18 = new QLabel(centralWidget);
        label_18->setObjectName(QStringLiteral("label_18"));

        horizontalLayout_12->addWidget(label_18);

        horizontalSpacer_21 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_12->addItem(horizontalSpacer_21);


        gridLayout_2->addLayout(horizontalLayout_12, 0, 3, 1, 1);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_10);

        ZAxisLabel = new QLabel(centralWidget);
        ZAxisLabel->setObjectName(QStringLiteral("ZAxisLabel"));

        horizontalLayout_6->addWidget(ZAxisLabel);

        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_11);


        gridLayout_2->addLayout(horizontalLayout_6, 0, 4, 1, 1);

        moveRelativeButton = new QPushButton(centralWidget);
        moveRelativeButton->setObjectName(QStringLiteral("moveRelativeButton"));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(moveRelativeButton->sizePolicy().hasHeightForWidth());
        moveRelativeButton->setSizePolicy(sizePolicy1);
        moveRelativeButton->setMinimumSize(QSize(93, 23));
        moveRelativeButton->setMaximumSize(QSize(130, 23));
        QFont font;
        font.setBold(false);
        font.setWeight(50);
        moveRelativeButton->setFont(font);

        gridLayout_2->addWidget(moveRelativeButton, 1, 0, 1, 1);

        moveRelativeX = new QLineEdit(centralWidget);
        moveRelativeX->setObjectName(QStringLiteral("moveRelativeX"));

        gridLayout_2->addWidget(moveRelativeX, 1, 1, 1, 1);

        moveRelativeY = new QLineEdit(centralWidget);
        moveRelativeY->setObjectName(QStringLiteral("moveRelativeY"));

        gridLayout_2->addWidget(moveRelativeY, 1, 2, 1, 1);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        horizontalSpacer_14 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_14);

        moveZButton = new QPushButton(centralWidget);
        moveZButton->setObjectName(QStringLiteral("moveZButton"));
        moveZButton->setMinimumSize(QSize(124, 23));
        moveZButton->setMaximumSize(QSize(124, 23));

        horizontalLayout_8->addWidget(moveZButton);

        horizontalSpacer_15 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_15);


        gridLayout_2->addLayout(horizontalLayout_8, 1, 4, 1, 1);

        moveIncrementButton = new QPushButton(centralWidget);
        moveIncrementButton->setObjectName(QStringLiteral("moveIncrementButton"));
        moveIncrementButton->setMinimumSize(QSize(93, 23));
        moveIncrementButton->setMaximumSize(QSize(134, 23));

        gridLayout_2->addWidget(moveIncrementButton, 2, 0, 1, 1);

        moveIncrementX = new QLineEdit(centralWidget);
        moveIncrementX->setObjectName(QStringLiteral("moveIncrementX"));

        gridLayout_2->addWidget(moveIncrementX, 2, 1, 1, 1);

        moveIncrementY = new QLineEdit(centralWidget);
        moveIncrementY->setObjectName(QStringLiteral("moveIncrementY"));

        gridLayout_2->addWidget(moveIncrementY, 2, 2, 1, 1);

        otherMovementFunctionsBox = new QGroupBox(centralWidget);
        otherMovementFunctionsBox->setObjectName(QStringLiteral("otherMovementFunctionsBox"));
        verticalLayout_6 = new QVBoxLayout(otherMovementFunctionsBox);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setSpacing(6);
        horizontalLayout_16->setObjectName(QStringLiteral("horizontalLayout_16"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        moveHomeButton = new QPushButton(otherMovementFunctionsBox);
        moveHomeButton->setObjectName(QStringLiteral("moveHomeButton"));
        QSizePolicy sizePolicy2(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(moveHomeButton->sizePolicy().hasHeightForWidth());
        moveHomeButton->setSizePolicy(sizePolicy2);
        moveHomeButton->setMinimumSize(QSize(91, 23));
        moveHomeButton->setMaximumSize(QSize(115, 23));
        moveHomeButton->setSizeIncrement(QSize(0, 0));
        QFont font1;
        font1.setPointSize(13);
        moveHomeButton->setFont(font1);

        verticalLayout_5->addWidget(moveHomeButton);

        setHomeButton = new QPushButton(otherMovementFunctionsBox);
        setHomeButton->setObjectName(QStringLiteral("setHomeButton"));
        setHomeButton->setMinimumSize(QSize(91, 23));
        setHomeButton->setMaximumSize(QSize(115, 23));

        verticalLayout_5->addWidget(setHomeButton);

        setContactButton = new QPushButton(otherMovementFunctionsBox);
        setContactButton->setObjectName(QStringLiteral("setContactButton"));
        setContactButton->setMinimumSize(QSize(91, 23));
        setContactButton->setMaximumSize(QSize(115, 23));
        setContactButton->setFont(font1);

        verticalLayout_5->addWidget(setContactButton);

        setSeparateButton = new QPushButton(otherMovementFunctionsBox);
        setSeparateButton->setObjectName(QStringLiteral("setSeparateButton"));
        setSeparateButton->setMinimumSize(QSize(91, 23));
        setSeparateButton->setMaximumSize(QSize(115, 23));
        setSeparateButton->setFont(font1);

        verticalLayout_5->addWidget(setSeparateButton);


        horizontalLayout_16->addLayout(verticalLayout_5);


        verticalLayout_6->addLayout(horizontalLayout_16);


        gridLayout_2->addWidget(otherMovementFunctionsBox, 2, 4, 7, 1);

        setIncrementButton = new QPushButton(centralWidget);
        setIncrementButton->setObjectName(QStringLiteral("setIncrementButton"));
        setIncrementButton->setMinimumSize(QSize(93, 23));
        setIncrementButton->setMaximumSize(QSize(130, 23));

        gridLayout_2->addWidget(setIncrementButton, 3, 0, 1, 1);

        setIncrementX = new QLineEdit(centralWidget);
        setIncrementX->setObjectName(QStringLiteral("setIncrementX"));

        gridLayout_2->addWidget(setIncrementX, 3, 1, 1, 1);

        setIncrementY = new QLineEdit(centralWidget);
        setIncrementY->setObjectName(QStringLiteral("setIncrementY"));

        gridLayout_2->addWidget(setIncrementY, 3, 2, 1, 1);

        currentIncrementLabel = new QLabel(centralWidget);
        currentIncrementLabel->setObjectName(QStringLiteral("currentIncrementLabel"));

        gridLayout_2->addWidget(currentIncrementLabel, 4, 0, 1, 1);

        currentXIncrementBox = new QLineEdit(centralWidget);
        currentXIncrementBox->setObjectName(QStringLiteral("currentXIncrementBox"));
        currentXIncrementBox->setReadOnly(true);

        gridLayout_2->addWidget(currentXIncrementBox, 4, 1, 1, 1);

        currentYIncrementBox = new QLineEdit(centralWidget);
        currentYIncrementBox->setObjectName(QStringLiteral("currentYIncrementBox"));
        currentYIncrementBox->setReadOnly(true);

        gridLayout_2->addWidget(currentYIncrementBox, 4, 2, 1, 1);

        currentLocationLabel = new QLabel(centralWidget);
        currentLocationLabel->setObjectName(QStringLiteral("currentLocationLabel"));

        gridLayout_2->addWidget(currentLocationLabel, 5, 0, 1, 1);

        currentXLocationBox = new QLineEdit(centralWidget);
        currentXLocationBox->setObjectName(QStringLiteral("currentXLocationBox"));
        currentXLocationBox->setReadOnly(true);

        gridLayout_2->addWidget(currentXLocationBox, 5, 1, 1, 1);

        currentYLocationBox = new QLineEdit(centralWidget);
        currentYLocationBox->setObjectName(QStringLiteral("currentYLocationBox"));
        currentYLocationBox->setReadOnly(true);

        gridLayout_2->addWidget(currentYLocationBox, 5, 2, 1, 1);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        horizontalSpacer_13 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_13);

        columnLabel = new QLabel(centralWidget);
        columnLabel->setObjectName(QStringLiteral("columnLabel"));

        horizontalLayout_11->addWidget(columnLabel);

        currentColumnBox = new QLineEdit(centralWidget);
        currentColumnBox->setObjectName(QStringLiteral("currentColumnBox"));
        currentColumnBox->setMinimumSize(QSize(81, 20));
        currentColumnBox->setMaximumSize(QSize(81, 20));
        currentColumnBox->setReadOnly(true);

        horizontalLayout_11->addWidget(currentColumnBox);


        gridLayout_2->addLayout(horizontalLayout_11, 6, 1, 1, 1);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_12);

        rowLabel = new QLabel(centralWidget);
        rowLabel->setObjectName(QStringLiteral("rowLabel"));

        horizontalLayout_7->addWidget(rowLabel);

        currentRowBox = new QLineEdit(centralWidget);
        currentRowBox->setObjectName(QStringLiteral("currentRowBox"));
        currentRowBox->setMinimumSize(QSize(81, 20));
        currentRowBox->setMaximumSize(QSize(81, 20));
        currentRowBox->setReadOnly(true);

        horizontalLayout_7->addWidget(currentRowBox);


        gridLayout_2->addLayout(horizontalLayout_7, 6, 2, 1, 1);

        currentChipLabel = new QLabel(centralWidget);
        currentChipLabel->setObjectName(QStringLiteral("currentChipLabel"));

        gridLayout_2->addWidget(currentChipLabel, 7, 0, 1, 1);

        currentChipBox = new QLineEdit(centralWidget);
        currentChipBox->setObjectName(QStringLiteral("currentChipBox"));
        currentChipBox->setMinimumSize(QSize(0, 20));
        currentChipBox->setMaximumSize(QSize(271, 20));
        currentChipBox->setReadOnly(true);

        gridLayout_2->addWidget(currentChipBox, 7, 1, 1, 1);

        moveToChipButton = new QPushButton(centralWidget);
        moveToChipButton->setObjectName(QStringLiteral("moveToChipButton"));
        sizePolicy1.setHeightForWidth(moveToChipButton->sizePolicy().hasHeightForWidth());
        moveToChipButton->setSizePolicy(sizePolicy1);
        moveToChipButton->setMinimumSize(QSize(75, 23));
        moveToChipButton->setMaximumSize(QSize(128, 23));

        gridLayout_2->addWidget(moveToChipButton, 8, 0, 1, 1);

        moveToChipBox = new QLineEdit(centralWidget);
        moveToChipBox->setObjectName(QStringLiteral("moveToChipBox"));
        moveToChipBox->setMinimumSize(QSize(0, 20));
        moveToChipBox->setMaximumSize(QSize(271, 20));

        gridLayout_2->addWidget(moveToChipBox, 8, 1, 1, 1);

        currentTLocationBox = new QLineEdit(centralWidget);
        currentTLocationBox->setObjectName(QStringLiteral("currentTLocationBox"));

        gridLayout_2->addWidget(currentTLocationBox, 5, 3, 1, 1);


        verticalLayout_4->addLayout(gridLayout_2);


        horizontalLayout_13->addLayout(verticalLayout_4);

        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setMinimumSize(QSize(341, 731));
        proberInfoTab = new QWidget();
        proberInfoTab->setObjectName(QStringLiteral("proberInfoTab"));
        verticalLayout_7 = new QVBoxLayout(proberInfoTab);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setSpacing(6);
        horizontalLayout_17->setObjectName(QStringLiteral("horizontalLayout_17"));
        horizontalSpacer_28 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_17->addItem(horizontalSpacer_28);

        connectProberButton = new QPushButton(proberInfoTab);
        connectProberButton->setObjectName(QStringLiteral("connectProberButton"));
        connectProberButton->setMinimumSize(QSize(151, 40));
        connectProberButton->setMaximumSize(QSize(151, 40));
        QFont font2;
        font2.setPointSize(12);
        font2.setBold(true);
        font2.setWeight(75);
        connectProberButton->setFont(font2);

        horizontalLayout_17->addWidget(connectProberButton);

        horizontalSpacer_29 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_17->addItem(horizontalSpacer_29);


        verticalLayout_7->addLayout(horizontalLayout_17);

        gridLayout_7 = new QGridLayout();
        gridLayout_7->setSpacing(6);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        label_3 = new QLabel(proberInfoTab);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_7->addWidget(label_3, 0, 0, 1, 1);

        baudRateBox = new QComboBox(proberInfoTab);
        baudRateBox->setObjectName(QStringLiteral("baudRateBox"));

        gridLayout_7->addWidget(baudRateBox, 0, 1, 1, 1);

        label_13 = new QLabel(proberInfoTab);
        label_13->setObjectName(QStringLiteral("label_13"));

        gridLayout_7->addWidget(label_13, 1, 0, 1, 1);

        parityBox = new QComboBox(proberInfoTab);
        parityBox->setObjectName(QStringLiteral("parityBox"));

        gridLayout_7->addWidget(parityBox, 1, 1, 1, 1);

        label_14 = new QLabel(proberInfoTab);
        label_14->setObjectName(QStringLiteral("label_14"));

        gridLayout_7->addWidget(label_14, 2, 0, 1, 1);

        deviceNameBox = new QLineEdit(proberInfoTab);
        deviceNameBox->setObjectName(QStringLiteral("deviceNameBox"));

        gridLayout_7->addWidget(deviceNameBox, 2, 1, 1, 1);

        label_15 = new QLabel(proberInfoTab);
        label_15->setObjectName(QStringLiteral("label_15"));

        gridLayout_7->addWidget(label_15, 3, 0, 1, 1);

        stopBitsBox = new QComboBox(proberInfoTab);
        stopBitsBox->setObjectName(QStringLiteral("stopBitsBox"));

        gridLayout_7->addWidget(stopBitsBox, 3, 1, 1, 1);

        label_16 = new QLabel(proberInfoTab);
        label_16->setObjectName(QStringLiteral("label_16"));

        gridLayout_7->addWidget(label_16, 4, 0, 1, 1);

        dataBitsBox = new QComboBox(proberInfoTab);
        dataBitsBox->setObjectName(QStringLiteral("dataBitsBox"));

        gridLayout_7->addWidget(dataBitsBox, 4, 1, 1, 1);

        label_17 = new QLabel(proberInfoTab);
        label_17->setObjectName(QStringLiteral("label_17"));

        gridLayout_7->addWidget(label_17, 5, 0, 1, 1);

        flowControlBox = new QComboBox(proberInfoTab);
        flowControlBox->setObjectName(QStringLiteral("flowControlBox"));

        gridLayout_7->addWidget(flowControlBox, 5, 1, 1, 1);


        verticalLayout_7->addLayout(gridLayout_7);

        horizontalLayout_20 = new QHBoxLayout();
        horizontalLayout_20->setSpacing(6);
        horizontalLayout_20->setObjectName(QStringLiteral("horizontalLayout_20"));
        horizontalSpacer_26 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_20->addItem(horizontalSpacer_26);

        setConnectionButton = new QPushButton(proberInfoTab);
        setConnectionButton->setObjectName(QStringLiteral("setConnectionButton"));
        setConnectionButton->setMinimumSize(QSize(141, 23));
        setConnectionButton->setMaximumSize(QSize(141, 23));

        horizontalLayout_20->addWidget(setConnectionButton);

        horizontalSpacer_27 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_20->addItem(horizontalSpacer_27);


        verticalLayout_7->addLayout(horizontalLayout_20);

        horizontalLayout_22 = new QHBoxLayout();
        horizontalLayout_22->setSpacing(6);
        horizontalLayout_22->setObjectName(QStringLiteral("horizontalLayout_22"));
        horizontalSpacer_32 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_22->addItem(horizontalSpacer_32);

        peekButton = new QPushButton(proberInfoTab);
        peekButton->setObjectName(QStringLiteral("peekButton"));

        horizontalLayout_22->addWidget(peekButton);

        readBufferButton = new QPushButton(proberInfoTab);
        readBufferButton->setObjectName(QStringLiteral("readBufferButton"));

        horizontalLayout_22->addWidget(readBufferButton);

        horizontalSpacer_33 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_22->addItem(horizontalSpacer_33);


        verticalLayout_7->addLayout(horizontalLayout_22);

        verticalSpacer_14 = new QSpacerItem(20, 460, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer_14);

        tabWidget->addTab(proberInfoTab, QString());
        otherFunctionsTab = new QWidget();
        otherFunctionsTab->setObjectName(QStringLiteral("otherFunctionsTab"));
        verticalLayout = new QVBoxLayout(otherFunctionsTab);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(-1);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        horizontalLayout_9->setSizeConstraint(QLayout::SetMaximumSize);
        clearOutputHistoryButton = new QPushButton(otherFunctionsTab);
        clearOutputHistoryButton->setObjectName(QStringLiteral("clearOutputHistoryButton"));
        clearOutputHistoryButton->setMinimumSize(QSize(118, 23));
        clearOutputHistoryButton->setMaximumSize(QSize(150, 23));

        horizontalLayout_9->addWidget(clearOutputHistoryButton);

        clearInputHistoryButton = new QPushButton(otherFunctionsTab);
        clearInputHistoryButton->setObjectName(QStringLiteral("clearInputHistoryButton"));
        clearInputHistoryButton->setMinimumSize(QSize(118, 23));
        clearInputHistoryButton->setMaximumSize(QSize(141, 23));

        horizontalLayout_9->addWidget(clearInputHistoryButton);


        verticalLayout->addLayout(horizontalLayout_9);

        clearMsgBox = new QPushButton(otherFunctionsTab);
        clearMsgBox->setObjectName(QStringLiteral("clearMsgBox"));
        QSizePolicy sizePolicy3(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(clearMsgBox->sizePolicy().hasHeightForWidth());
        clearMsgBox->setSizePolicy(sizePolicy3);
        clearMsgBox->setMinimumSize(QSize(118, 23));
        clearMsgBox->setMaximumSize(QSize(150, 23));
        clearMsgBox->setLayoutDirection(Qt::LeftToRight);

        verticalLayout->addWidget(clearMsgBox);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        horizontalSpacer_16 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_16);

        refreshLocationButton = new QPushButton(otherFunctionsTab);
        refreshLocationButton->setObjectName(QStringLiteral("refreshLocationButton"));
        sizePolicy1.setHeightForWidth(refreshLocationButton->sizePolicy().hasHeightForWidth());
        refreshLocationButton->setSizePolicy(sizePolicy1);
        refreshLocationButton->setMinimumSize(QSize(118, 23));
        refreshLocationButton->setMaximumSize(QSize(133, 23));

        horizontalLayout_10->addWidget(refreshLocationButton);

        horizontalSpacer_18 = new QSpacerItem(40, 20, QSizePolicy::Preferred, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_18);


        verticalLayout->addLayout(horizontalLayout_10);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setSpacing(6);
        horizontalLayout_15->setObjectName(QStringLiteral("horizontalLayout_15"));
        horizontalSpacer_24 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_15->addItem(horizontalSpacer_24);

        resetIncrementButton = new QPushButton(otherFunctionsTab);
        resetIncrementButton->setObjectName(QStringLiteral("resetIncrementButton"));

        horizontalLayout_15->addWidget(resetIncrementButton);

        horizontalSpacer_25 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_15->addItem(horizontalSpacer_25);


        verticalLayout->addLayout(horizontalLayout_15);

        horizontalLayout_21 = new QHBoxLayout();
        horizontalLayout_21->setSpacing(6);
        horizontalLayout_21->setObjectName(QStringLiteral("horizontalLayout_21"));
        horizontalSpacer_30 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_21->addItem(horizontalSpacer_30);

        enableStartupToolsButton = new QPushButton(otherFunctionsTab);
        enableStartupToolsButton->setObjectName(QStringLiteral("enableStartupToolsButton"));

        horizontalLayout_21->addWidget(enableStartupToolsButton);

        horizontalSpacer_31 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_21->addItem(horizontalSpacer_31);


        verticalLayout->addLayout(horizontalLayout_21);

        startupToolsGroup = new QGroupBox(otherFunctionsTab);
        startupToolsGroup->setObjectName(QStringLiteral("startupToolsGroup"));
        startupToolsGroup->setEnabled(false);
        gridLayout_10 = new QGridLayout(startupToolsGroup);
        gridLayout_10->setSpacing(6);
        gridLayout_10->setContentsMargins(11, 11, 11, 11);
        gridLayout_10->setObjectName(QStringLiteral("gridLayout_10"));
        gridLayout_4 = new QGridLayout();
        gridLayout_4->setSpacing(6);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        positionReportingOffButton = new QPushButton(startupToolsGroup);
        positionReportingOffButton->setObjectName(QStringLiteral("positionReportingOffButton"));

        gridLayout_4->addWidget(positionReportingOffButton, 0, 0, 1, 2);

        setTVeloButton = new QPushButton(startupToolsGroup);
        setTVeloButton->setObjectName(QStringLiteral("setTVeloButton"));

        gridLayout_4->addWidget(setTVeloButton, 1, 0, 1, 1);

        setXVeloButton = new QPushButton(startupToolsGroup);
        setXVeloButton->setObjectName(QStringLiteral("setXVeloButton"));

        gridLayout_4->addWidget(setXVeloButton, 2, 0, 1, 1);

        setYVeloButton = new QPushButton(startupToolsGroup);
        setYVeloButton->setObjectName(QStringLiteral("setYVeloButton"));

        gridLayout_4->addWidget(setYVeloButton, 3, 0, 1, 1);

        initializeButton = new QPushButton(startupToolsGroup);
        initializeButton->setObjectName(QStringLiteral("initializeButton"));

        gridLayout_4->addWidget(initializeButton, 4, 0, 1, 2);

        setUnitsButton = new QPushButton(startupToolsGroup);
        setUnitsButton->setObjectName(QStringLiteral("setUnitsButton"));

        gridLayout_4->addWidget(setUnitsButton, 5, 0, 1, 1);

        unitsBox = new QComboBox(startupToolsGroup);
        unitsBox->setObjectName(QStringLiteral("unitsBox"));

        gridLayout_4->addWidget(unitsBox, 5, 1, 1, 1);

        setWaferSizeButton = new QPushButton(startupToolsGroup);
        setWaferSizeButton->setObjectName(QStringLiteral("setWaferSizeButton"));

        gridLayout_4->addWidget(setWaferSizeButton, 6, 0, 1, 1);

        tVeloBox = new QDoubleSpinBox(startupToolsGroup);
        tVeloBox->setObjectName(QStringLiteral("tVeloBox"));
        tVeloBox->setValue(1);

        gridLayout_4->addWidget(tVeloBox, 1, 1, 1, 1);

        xVeloBox = new QDoubleSpinBox(startupToolsGroup);
        xVeloBox->setObjectName(QStringLiteral("xVeloBox"));
        xVeloBox->setValue(4);

        gridLayout_4->addWidget(xVeloBox, 2, 1, 1, 1);

        yVeloBox = new QDoubleSpinBox(startupToolsGroup);
        yVeloBox->setObjectName(QStringLiteral("yVeloBox"));
        yVeloBox->setValue(4);

        gridLayout_4->addWidget(yVeloBox, 3, 1, 1, 1);

        waferSizeBox = new QDoubleSpinBox(startupToolsGroup);
        waferSizeBox->setObjectName(QStringLiteral("waferSizeBox"));
        waferSizeBox->setMaximum(200);
        waferSizeBox->setValue(200);

        gridLayout_4->addWidget(waferSizeBox, 6, 1, 1, 1);


        gridLayout_10->addLayout(gridLayout_4, 0, 0, 1, 1);


        verticalLayout->addWidget(startupToolsGroup);

        verticalSpacer = new QSpacerItem(17, 560, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        tabWidget->addTab(otherFunctionsTab, QString());
        waferTab = new QWidget();
        waferTab->setObjectName(QStringLiteral("waferTab"));
        verticalLayout_3 = new QVBoxLayout(waferTab);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        alignButton1 = new QPushButton(waferTab);
        alignButton1->setObjectName(QStringLiteral("alignButton1"));
        alignButton1->setMinimumSize(QSize(121, 23));
        alignButton1->setMaximumSize(QSize(121, 23));

        gridLayout_3->addWidget(alignButton1, 0, 0, 1, 1);

        gridLayout_8 = new QGridLayout();
        gridLayout_8->setSpacing(6);
        gridLayout_8->setObjectName(QStringLiteral("gridLayout_8"));
        label_7 = new QLabel(waferTab);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout_8->addWidget(label_7, 0, 0, 1, 1);

        alignLocationXBox1 = new QLineEdit(waferTab);
        alignLocationXBox1->setObjectName(QStringLiteral("alignLocationXBox1"));
        alignLocationXBox1->setReadOnly(true);

        gridLayout_8->addWidget(alignLocationXBox1, 0, 1, 1, 1);

        label_8 = new QLabel(waferTab);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout_8->addWidget(label_8, 1, 0, 1, 1);

        alignLocationYBox1 = new QLineEdit(waferTab);
        alignLocationYBox1->setObjectName(QStringLiteral("alignLocationYBox1"));
        alignLocationYBox1->setReadOnly(true);

        gridLayout_8->addWidget(alignLocationYBox1, 1, 1, 1, 1);

        label_9 = new QLabel(waferTab);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout_8->addWidget(label_9, 2, 0, 1, 1);

        alignLocationTBox1 = new QLineEdit(waferTab);
        alignLocationTBox1->setObjectName(QStringLiteral("alignLocationTBox1"));
        alignLocationTBox1->setReadOnly(true);

        gridLayout_8->addWidget(alignLocationTBox1, 2, 1, 1, 1);


        gridLayout_3->addLayout(gridLayout_8, 0, 1, 2, 1);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_3->addItem(verticalSpacer_3, 1, 0, 1, 1);


        verticalLayout_2->addLayout(gridLayout_3);

        verticalSpacer_4 = new QSpacerItem(20, 13, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_4);

        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setSpacing(6);
        horizontalLayout_18->setObjectName(QStringLiteral("horizontalLayout_18"));
        alignButton2 = new QPushButton(waferTab);
        alignButton2->setObjectName(QStringLiteral("alignButton2"));
        alignButton2->setEnabled(false);
        alignButton2->setMinimumSize(QSize(121, 23));
        alignButton2->setMaximumSize(QSize(121, 23));
        QFont font3;
        font3.setPointSize(10);
        alignButton2->setFont(font3);

        horizontalLayout_18->addWidget(alignButton2);

        label = new QLabel(waferTab);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_18->addWidget(label);

        alignMoveIncrementXBox = new QLineEdit(waferTab);
        alignMoveIncrementXBox->setObjectName(QStringLiteral("alignMoveIncrementXBox"));

        horizontalLayout_18->addWidget(alignMoveIncrementXBox);


        verticalLayout_2->addLayout(horizontalLayout_18);

        verticalSpacer_5 = new QSpacerItem(20, 13, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_5);

        gridLayout_16 = new QGridLayout();
        gridLayout_16->setSpacing(6);
        gridLayout_16->setObjectName(QStringLiteral("gridLayout_16"));
        alignButton3 = new QPushButton(waferTab);
        alignButton3->setObjectName(QStringLiteral("alignButton3"));
        alignButton3->setEnabled(false);
        alignButton3->setMinimumSize(QSize(121, 23));
        alignButton3->setMaximumSize(QSize(121, 23));
        QFont font4;
        font4.setPointSize(12);
        alignButton3->setFont(font4);

        gridLayout_16->addWidget(alignButton3, 0, 0, 1, 1);

        gridLayout_5 = new QGridLayout();
        gridLayout_5->setSpacing(6);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        label_4 = new QLabel(waferTab);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout_5->addWidget(label_4, 0, 0, 1, 1);

        alignLocationXBox2 = new QLineEdit(waferTab);
        alignLocationXBox2->setObjectName(QStringLiteral("alignLocationXBox2"));
        alignLocationXBox2->setReadOnly(true);

        gridLayout_5->addWidget(alignLocationXBox2, 0, 1, 1, 1);

        label_5 = new QLabel(waferTab);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout_5->addWidget(label_5, 1, 0, 1, 1);

        alignLocationYBox2 = new QLineEdit(waferTab);
        alignLocationYBox2->setObjectName(QStringLiteral("alignLocationYBox2"));
        alignLocationYBox2->setReadOnly(true);

        gridLayout_5->addWidget(alignLocationYBox2, 1, 1, 1, 1);

        label_6 = new QLabel(waferTab);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout_5->addWidget(label_6, 2, 0, 1, 1);

        alignLocationTBox2 = new QLineEdit(waferTab);
        alignLocationTBox2->setObjectName(QStringLiteral("alignLocationTBox2"));
        alignLocationTBox2->setReadOnly(true);

        gridLayout_5->addWidget(alignLocationTBox2, 2, 1, 1, 1);


        gridLayout_16->addLayout(gridLayout_5, 0, 1, 2, 1);

        verticalSpacer_6 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_16->addItem(verticalSpacer_6, 1, 0, 1, 1);


        verticalLayout_2->addLayout(gridLayout_16);

        verticalSpacer_9 = new QSpacerItem(20, 13, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_9);

        gridLayout_18 = new QGridLayout();
        gridLayout_18->setSpacing(6);
        gridLayout_18->setObjectName(QStringLiteral("gridLayout_18"));
        alignButton4 = new QPushButton(waferTab);
        alignButton4->setObjectName(QStringLiteral("alignButton4"));
        alignButton4->setEnabled(false);
        alignButton4->setMinimumSize(QSize(121, 23));
        alignButton4->setMaximumSize(QSize(121, 23));

        gridLayout_18->addWidget(alignButton4, 0, 0, 1, 1);

        gridLayout_17 = new QGridLayout();
        gridLayout_17->setSpacing(6);
        gridLayout_17->setObjectName(QStringLiteral("gridLayout_17"));
        label_2 = new QLabel(waferTab);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout_17->addWidget(label_2, 0, 0, 1, 1);

        alignMoveRelativeXBox = new QLineEdit(waferTab);
        alignMoveRelativeXBox->setObjectName(QStringLiteral("alignMoveRelativeXBox"));

        gridLayout_17->addWidget(alignMoveRelativeXBox, 0, 1, 1, 1);

        label_23 = new QLabel(waferTab);
        label_23->setObjectName(QStringLiteral("label_23"));

        gridLayout_17->addWidget(label_23, 1, 0, 1, 1);

        alignMoveRelativeYBox = new QLineEdit(waferTab);
        alignMoveRelativeYBox->setObjectName(QStringLiteral("alignMoveRelativeYBox"));

        gridLayout_17->addWidget(alignMoveRelativeYBox, 1, 1, 1, 1);


        gridLayout_18->addLayout(gridLayout_17, 0, 1, 2, 1);

        verticalSpacer_7 = new QSpacerItem(20, 18, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_18->addItem(verticalSpacer_7, 1, 0, 1, 1);


        verticalLayout_2->addLayout(gridLayout_18);

        verticalSpacer_10 = new QSpacerItem(20, 13, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_10);

        gridLayout_19 = new QGridLayout();
        gridLayout_19->setSpacing(6);
        gridLayout_19->setObjectName(QStringLiteral("gridLayout_19"));
        alignButton5 = new QPushButton(waferTab);
        alignButton5->setObjectName(QStringLiteral("alignButton5"));
        alignButton5->setEnabled(false);
        alignButton5->setMinimumSize(QSize(121, 23));
        alignButton5->setMaximumSize(QSize(121, 23));

        gridLayout_19->addWidget(alignButton5, 0, 0, 1, 1);

        gridLayout_9 = new QGridLayout();
        gridLayout_9->setSpacing(6);
        gridLayout_9->setObjectName(QStringLiteral("gridLayout_9"));
        label_10 = new QLabel(waferTab);
        label_10->setObjectName(QStringLiteral("label_10"));

        gridLayout_9->addWidget(label_10, 0, 0, 1, 1);

        alignLocationXBox3 = new QLineEdit(waferTab);
        alignLocationXBox3->setObjectName(QStringLiteral("alignLocationXBox3"));
        alignLocationXBox3->setReadOnly(true);

        gridLayout_9->addWidget(alignLocationXBox3, 0, 1, 1, 1);

        label_11 = new QLabel(waferTab);
        label_11->setObjectName(QStringLiteral("label_11"));

        gridLayout_9->addWidget(label_11, 1, 0, 1, 1);

        alignLocationYBox3 = new QLineEdit(waferTab);
        alignLocationYBox3->setObjectName(QStringLiteral("alignLocationYBox3"));
        alignLocationYBox3->setReadOnly(true);

        gridLayout_9->addWidget(alignLocationYBox3, 1, 1, 1, 1);

        label_12 = new QLabel(waferTab);
        label_12->setObjectName(QStringLiteral("label_12"));

        gridLayout_9->addWidget(label_12, 2, 0, 1, 1);

        alignLocationTBox3 = new QLineEdit(waferTab);
        alignLocationTBox3->setObjectName(QStringLiteral("alignLocationTBox3"));
        alignLocationTBox3->setReadOnly(true);

        gridLayout_9->addWidget(alignLocationTBox3, 2, 1, 1, 1);


        gridLayout_19->addLayout(gridLayout_9, 0, 1, 2, 1);

        verticalSpacer_8 = new QSpacerItem(20, 28, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_19->addItem(verticalSpacer_8, 1, 0, 1, 1);


        verticalLayout_2->addLayout(gridLayout_19);

        verticalSpacer_11 = new QSpacerItem(20, 13, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_11);

        horizontalLayout_19 = new QHBoxLayout();
        horizontalLayout_19->setSpacing(6);
        horizontalLayout_19->setObjectName(QStringLiteral("horizontalLayout_19"));
        alignButton6 = new QPushButton(waferTab);
        alignButton6->setObjectName(QStringLiteral("alignButton6"));
        alignButton6->setEnabled(false);
        alignButton6->setMinimumSize(QSize(121, 23));
        alignButton6->setMaximumSize(QSize(121, 23));

        horizontalLayout_19->addWidget(alignButton6);

        alignComputeRotationBox = new QLineEdit(waferTab);
        alignComputeRotationBox->setObjectName(QStringLiteral("alignComputeRotationBox"));
        alignComputeRotationBox->setReadOnly(true);

        horizontalLayout_19->addWidget(alignComputeRotationBox);


        verticalLayout_2->addLayout(horizontalLayout_19);

        alignButton7 = new QPushButton(waferTab);
        alignButton7->setObjectName(QStringLiteral("alignButton7"));
        alignButton7->setEnabled(false);
        alignButton7->setMinimumSize(QSize(121, 23));
        alignButton7->setMaximumSize(QSize(121, 23));

        verticalLayout_2->addWidget(alignButton7);


        verticalLayout_3->addLayout(verticalLayout_2);

        verticalSpacer_12 = new QSpacerItem(20, 79, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_12);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setSpacing(6);
        horizontalLayout_14->setObjectName(QStringLiteral("horizontalLayout_14"));
        horizontalSpacer_22 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_14->addItem(horizontalSpacer_22);

        alignmentLabel = new QLabel(waferTab);
        alignmentLabel->setObjectName(QStringLiteral("alignmentLabel"));
        alignmentLabel->setMinimumSize(QSize(0, 0));

        horizontalLayout_14->addWidget(alignmentLabel);

        horizontalSpacer_23 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_14->addItem(horizontalSpacer_23);


        verticalLayout_3->addLayout(horizontalLayout_14);

        verticalSpacer_13 = new QSpacerItem(20, 78, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_13);

        tabWidget->addTab(waferTab, QString());
        libraryTab = new QWidget();
        libraryTab->setObjectName(QStringLiteral("libraryTab"));
        gridLayout_6 = new QGridLayout(libraryTab);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        treeWidget = new QTreeWidget(libraryTab);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem(treeWidget);
        QTreeWidgetItem *__qtreewidgetitem1 = new QTreeWidgetItem(__qtreewidgetitem);
        QTreeWidgetItem *__qtreewidgetitem2 = new QTreeWidgetItem(__qtreewidgetitem1);
        new QTreeWidgetItem(__qtreewidgetitem2);
        QTreeWidgetItem *__qtreewidgetitem3 = new QTreeWidgetItem(__qtreewidgetitem1);
        QTreeWidgetItem *__qtreewidgetitem4 = new QTreeWidgetItem(__qtreewidgetitem3);
        new QTreeWidgetItem(__qtreewidgetitem4);
        QTreeWidgetItem *__qtreewidgetitem5 = new QTreeWidgetItem(__qtreewidgetitem3);
        new QTreeWidgetItem(__qtreewidgetitem5);
        QTreeWidgetItem *__qtreewidgetitem6 = new QTreeWidgetItem(__qtreewidgetitem);
        QTreeWidgetItem *__qtreewidgetitem7 = new QTreeWidgetItem(__qtreewidgetitem6);
        new QTreeWidgetItem(__qtreewidgetitem7);
        QTreeWidgetItem *__qtreewidgetitem8 = new QTreeWidgetItem(__qtreewidgetitem6);
        new QTreeWidgetItem(__qtreewidgetitem8);
        QTreeWidgetItem *__qtreewidgetitem9 = new QTreeWidgetItem(__qtreewidgetitem);
        QTreeWidgetItem *__qtreewidgetitem10 = new QTreeWidgetItem(__qtreewidgetitem9);
        new QTreeWidgetItem(__qtreewidgetitem10);
        QTreeWidgetItem *__qtreewidgetitem11 = new QTreeWidgetItem(__qtreewidgetitem9);
        new QTreeWidgetItem(__qtreewidgetitem11);
        QTreeWidgetItem *__qtreewidgetitem12 = new QTreeWidgetItem(__qtreewidgetitem9);
        QTreeWidgetItem *__qtreewidgetitem13 = new QTreeWidgetItem(__qtreewidgetitem12);
        new QTreeWidgetItem(__qtreewidgetitem13);
        QTreeWidgetItem *__qtreewidgetitem14 = new QTreeWidgetItem(__qtreewidgetitem12);
        new QTreeWidgetItem(__qtreewidgetitem14);
        QTreeWidgetItem *__qtreewidgetitem15 = new QTreeWidgetItem(treeWidget);
        QTreeWidgetItem *__qtreewidgetitem16 = new QTreeWidgetItem(__qtreewidgetitem15);
        new QTreeWidgetItem(__qtreewidgetitem16);
        new QTreeWidgetItem(__qtreewidgetitem16);
        QTreeWidgetItem *__qtreewidgetitem17 = new QTreeWidgetItem(__qtreewidgetitem15);
        new QTreeWidgetItem(__qtreewidgetitem17);
        new QTreeWidgetItem(__qtreewidgetitem17);
        treeWidget->setObjectName(QStringLiteral("treeWidget"));
        treeWidget->setItemsExpandable(true);

        gridLayout_6->addWidget(treeWidget, 0, 0, 1, 1);

        tabWidget->addTab(libraryTab, QString());

        horizontalLayout_13->addWidget(tabWidget);


        verticalLayout_8->addLayout(horizontalLayout_13);

        msgBox = new QPlainTextEdit(centralWidget);
        msgBox->setObjectName(QStringLiteral("msgBox"));

        verticalLayout_8->addWidget(msgBox);

        WP->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(WP);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1094, 22));
        menuVersion_5_0 = new QMenu(menuBar);
        menuVersion_5_0->setObjectName(QStringLiteral("menuVersion_5_0"));
        WP->setMenuBar(menuBar);
        mainToolBar = new QToolBar(WP);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        WP->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(WP);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        WP->setStatusBar(statusBar);

        menuBar->addAction(menuVersion_5_0->menuAction());

        retranslateUi(WP);

        sendCommandButton->setDefault(false);
        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(WP);
    } // setupUi

    void retranslateUi(QMainWindow *WP)
    {
        WP->setWindowTitle(QApplication::translate("WP", "WP", Q_NULLPTR));
        inputHistoryLabel->setText(QApplication::translate("WP", "Input History", Q_NULLPTR));
        probeStationOutputLabel->setText(QApplication::translate("WP", "Probe Station Output", Q_NULLPTR));
        enterCommandLabel->setText(QApplication::translate("WP", "Enter Command:", Q_NULLPTR));
        sendCommandButton->setText(QApplication::translate("WP", "Send Command", Q_NULLPTR));
        outputHistoryLog->setPlainText(QString());
        XAxisLabel->setText(QApplication::translate("WP", "X-Axis", Q_NULLPTR));
        YAxisLabel->setText(QApplication::translate("WP", "Y-Axis", Q_NULLPTR));
        label_18->setText(QApplication::translate("WP", "T-Axis", Q_NULLPTR));
        ZAxisLabel->setText(QApplication::translate("WP", "Z-Axis", Q_NULLPTR));
        moveRelativeButton->setText(QApplication::translate("WP", "Move Relative", Q_NULLPTR));
        moveZButton->setText(QApplication::translate("WP", "Move to Contact", Q_NULLPTR));
        moveIncrementButton->setText(QApplication::translate("WP", "Move Increment", Q_NULLPTR));
        otherMovementFunctionsBox->setTitle(QApplication::translate("WP", "Other Movement Functions", Q_NULLPTR));
        moveHomeButton->setText(QApplication::translate("WP", "Move Home", Q_NULLPTR));
        setHomeButton->setText(QApplication::translate("WP", "Set Home", Q_NULLPTR));
        setContactButton->setText(QApplication::translate("WP", "Set Contact", Q_NULLPTR));
        setSeparateButton->setText(QApplication::translate("WP", "Set Separate...", Q_NULLPTR));
        setIncrementButton->setText(QApplication::translate("WP", "Set Increment", Q_NULLPTR));
        currentIncrementLabel->setText(QApplication::translate("WP", "Current Increment:", Q_NULLPTR));
        currentLocationLabel->setText(QApplication::translate("WP", "Current Location: ", Q_NULLPTR));
        columnLabel->setText(QApplication::translate("WP", "Column: ", Q_NULLPTR));
        rowLabel->setText(QApplication::translate("WP", "Row:", Q_NULLPTR));
        currentChipLabel->setText(QApplication::translate("WP", "Current Chip:", Q_NULLPTR));
        moveToChipButton->setText(QApplication::translate("WP", "Move to Chip", Q_NULLPTR));
        connectProberButton->setText(QApplication::translate("WP", "Connect", Q_NULLPTR));
        label_3->setText(QApplication::translate("WP", "Baud Rate:", Q_NULLPTR));
        label_13->setText(QApplication::translate("WP", "Parity:", Q_NULLPTR));
        label_14->setText(QApplication::translate("WP", "Device Name (COM Port):", Q_NULLPTR));
        label_15->setText(QApplication::translate("WP", "Stop Bits:", Q_NULLPTR));
        label_16->setText(QApplication::translate("WP", "Data Bits:", Q_NULLPTR));
        label_17->setText(QApplication::translate("WP", "Flow Control:", Q_NULLPTR));
        setConnectionButton->setText(QApplication::translate("WP", "Set Connection Properties", Q_NULLPTR));
        peekButton->setText(QApplication::translate("WP", "Peek", Q_NULLPTR));
        readBufferButton->setText(QApplication::translate("WP", "ReadBuffer", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(proberInfoTab), QApplication::translate("WP", "Prober", Q_NULLPTR));
        clearOutputHistoryButton->setText(QApplication::translate("WP", "Clear Output History", Q_NULLPTR));
        clearInputHistoryButton->setText(QApplication::translate("WP", "Clear Input History", Q_NULLPTR));
        clearMsgBox->setText(QApplication::translate("WP", "Clear Messages", Q_NULLPTR));
        refreshLocationButton->setText(QApplication::translate("WP", "Refresh Location", Q_NULLPTR));
        resetIncrementButton->setText(QApplication::translate("WP", "Reset Increment to FE-I4 Default", Q_NULLPTR));
        enableStartupToolsButton->setText(QApplication::translate("WP", "Enable Startup Configuration Tools", Q_NULLPTR));
        startupToolsGroup->setTitle(QApplication::translate("WP", "Startup Configuration Tools", Q_NULLPTR));
        positionReportingOffButton->setText(QApplication::translate("WP", "Turn Off Position Reporting", Q_NULLPTR));
        setTVeloButton->setText(QApplication::translate("WP", "Set \316\230 Velocity (Deg/s)", Q_NULLPTR));
        setXVeloButton->setText(QApplication::translate("WP", "Set X Velocity (mm/s)", Q_NULLPTR));
        setYVeloButton->setText(QApplication::translate("WP", "Set Y Velocity (mm/s)", Q_NULLPTR));
        initializeButton->setText(QApplication::translate("WP", "Send Initialization Command", Q_NULLPTR));
        setUnitsButton->setText(QApplication::translate("WP", "Set Units", Q_NULLPTR));
        setWaferSizeButton->setText(QApplication::translate("WP", "Set Wafer Size (mm)", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(otherFunctionsTab), QApplication::translate("WP", "Other Functions", Q_NULLPTR));
        alignButton1->setText(QApplication::translate("WP", "Get 1st Location", Q_NULLPTR));
        label_7->setText(QApplication::translate("WP", "X:", Q_NULLPTR));
        label_8->setText(QApplication::translate("WP", "Y:", Q_NULLPTR));
        label_9->setText(QApplication::translate("WP", "T:", Q_NULLPTR));
        alignButton2->setText(QApplication::translate("WP", "Move to Other Side", Q_NULLPTR));
        label->setText(QApplication::translate("WP", "X:", Q_NULLPTR));
        alignButton3->setText(QApplication::translate("WP", "Get 2nd Location", Q_NULLPTR));
        label_4->setText(QApplication::translate("WP", "X:", Q_NULLPTR));
        label_5->setText(QApplication::translate("WP", "Y:", Q_NULLPTR));
        label_6->setText(QApplication::translate("WP", "T:", Q_NULLPTR));
        alignButton4->setText(QApplication::translate("WP", "Move Relative", Q_NULLPTR));
        label_2->setText(QApplication::translate("WP", "X:", Q_NULLPTR));
        label_23->setText(QApplication::translate("WP", "Y:", Q_NULLPTR));
        alignButton5->setText(QApplication::translate("WP", "Get 3rd Location", Q_NULLPTR));
        label_10->setText(QApplication::translate("WP", "X:", Q_NULLPTR));
        label_11->setText(QApplication::translate("WP", "Y:", Q_NULLPTR));
        label_12->setText(QApplication::translate("WP", "T:", Q_NULLPTR));
        alignButton6->setText(QApplication::translate("WP", "Compute Rotation \316\230", Q_NULLPTR));
        alignButton7->setText(QApplication::translate("WP", "Rotate DUT To Align", Q_NULLPTR));
        alignmentLabel->setText(QApplication::translate("WP", "Start alignment process by clicking Get 1st Location", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(waferTab), QApplication::translate("WP", "Align Wafer", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem = treeWidget->headerItem();
        ___qtreewidgetitem->setText(0, QApplication::translate("WP", "Library", Q_NULLPTR));

        const bool __sortingEnabled = treeWidget->isSortingEnabled();
        treeWidget->setSortingEnabled(false);
        QTreeWidgetItem *___qtreewidgetitem1 = treeWidget->topLevelItem(0);
        ___qtreewidgetitem1->setText(0, QApplication::translate("WP", "Prober Commands", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem2 = ___qtreewidgetitem1->child(0);
        ___qtreewidgetitem2->setText(0, QApplication::translate("WP", "Settings", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem3 = ___qtreewidgetitem2->child(0);
        ___qtreewidgetitem3->setText(0, QApplication::translate("WP", "Set Increment", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem4 = ___qtreewidgetitem3->child(0);
        ___qtreewidgetitem4->setText(0, QApplication::translate("WP", "SN D X [increment] Y [increment]", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        ___qtreewidgetitem4->setToolTip(0, QApplication::translate("WP", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">General form: SN &lt;channel&gt; [&lt;axis&gt; &lt;increment&gt; ...]</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Sets the amount the prober moves when Move Increment is used. Typically set to the chip-to-chip pitch.</span></p></body>"
                        "</html>", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        QTreeWidgetItem *___qtreewidgetitem5 = ___qtreewidgetitem2->child(1);
        ___qtreewidgetitem5->setText(0, QApplication::translate("WP", "Set Z Axis", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem6 = ___qtreewidgetitem5->child(0);
        ___qtreewidgetitem6->setText(0, QApplication::translate("WP", "Set Contact", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem7 = ___qtreewidgetitem6->child(0);
        ___qtreewidgetitem7->setText(0, QApplication::translate("WP", "SZ D C", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        ___qtreewidgetitem7->setToolTip(0, QApplication::translate("WP", "Sets the current z-axis position as the contact position. When the Move to Contact command is sent, the prober will move to the contact position unless it is already there.", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        QTreeWidgetItem *___qtreewidgetitem8 = ___qtreewidgetitem5->child(1);
        ___qtreewidgetitem8->setText(0, QApplication::translate("WP", "Set Separate", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem9 = ___qtreewidgetitem8->child(0);
        ___qtreewidgetitem9->setText(0, QApplication::translate("WP", "SZ D S [position (\302\265m)]", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        ___qtreewidgetitem9->setToolTip(0, QApplication::translate("WP", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Sets the current z-axis position as the separate position. When the Move to Separate command is sent, the prober will move to the separate position unless it is already there.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Additionally, "
                        "a position can be specified. Even though all other distances used by the prober are in mm, this position is in \302\265m, and it is always relative to the contact position.</span></p></body></html>", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        QTreeWidgetItem *___qtreewidgetitem10 = ___qtreewidgetitem1->child(1);
        ___qtreewidgetitem10->setText(0, QApplication::translate("WP", "Queries", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem11 = ___qtreewidgetitem10->child(0);
        ___qtreewidgetitem11->setText(0, QApplication::translate("WP", "Query Position", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem12 = ___qtreewidgetitem11->child(0);
        ___qtreewidgetitem12->setText(0, QApplication::translate("WP", "QP D", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem13 = ___qtreewidgetitem10->child(1);
        ___qtreewidgetitem13->setText(0, QApplication::translate("WP", "Query Increment", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem14 = ___qtreewidgetitem13->child(0);
        ___qtreewidgetitem14->setText(0, QApplication::translate("WP", "QN D", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem15 = ___qtreewidgetitem1->child(2);
        ___qtreewidgetitem15->setText(0, QApplication::translate("WP", "Movement", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem16 = ___qtreewidgetitem15->child(0);
        ___qtreewidgetitem16->setText(0, QApplication::translate("WP", "Move Relative", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem17 = ___qtreewidgetitem16->child(0);
        ___qtreewidgetitem17->setText(0, QApplication::translate("WP", "MR D X [distance] Y [distance]", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem18 = ___qtreewidgetitem15->child(1);
        ___qtreewidgetitem18->setText(0, QApplication::translate("WP", "Move Increment", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem19 = ___qtreewidgetitem18->child(0);
        ___qtreewidgetitem19->setText(0, QApplication::translate("WP", "MN D X [# of steps] Y [# of steps]", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem20 = ___qtreewidgetitem15->child(2);
        ___qtreewidgetitem20->setText(0, QApplication::translate("WP", "Move Z Axis", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem21 = ___qtreewidgetitem20->child(0);
        ___qtreewidgetitem21->setText(0, QApplication::translate("WP", "Move to Contact", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem22 = ___qtreewidgetitem21->child(0);
        ___qtreewidgetitem22->setText(0, QApplication::translate("WP", "MZ D C", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem23 = ___qtreewidgetitem20->child(1);
        ___qtreewidgetitem23->setText(0, QApplication::translate("WP", "Move to Separate", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem24 = ___qtreewidgetitem23->child(0);
        ___qtreewidgetitem24->setText(0, QApplication::translate("WP", "MZ D S", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem25 = treeWidget->topLevelItem(1);
        ___qtreewidgetitem25->setText(0, QApplication::translate("WP", "FE-I4 Information", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem26 = ___qtreewidgetitem25->child(0);
        ___qtreewidgetitem26->setText(0, QApplication::translate("WP", "Chip Pitch (Measured)", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem27 = ___qtreewidgetitem26->child(0);
        ___qtreewidgetitem27->setText(0, QApplication::translate("WP", "Chip to Chip Pitch X: 20.320228", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem28 = ___qtreewidgetitem26->child(1);
        ___qtreewidgetitem28->setText(0, QApplication::translate("WP", "Chip to Chip Pitch Y: 19.26256", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem29 = ___qtreewidgetitem25->child(1);
        ___qtreewidgetitem29->setText(0, QApplication::translate("WP", "Chip Pitch - Specified", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem30 = ___qtreewidgetitem29->child(0);
        ___qtreewidgetitem30->setText(0, QApplication::translate("WP", "Chip to Chip Pitch X: 20.33016", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem31 = ___qtreewidgetitem29->child(1);
        ___qtreewidgetitem31->setText(0, QApplication::translate("WP", "Chip to Chip Pitch Y: 19.26256", Q_NULLPTR));
        treeWidget->setSortingEnabled(__sortingEnabled);

        tabWidget->setTabText(tabWidget->indexOf(libraryTab), QApplication::translate("WP", "Library", Q_NULLPTR));
        menuVersion_5_0->setTitle(QApplication::translate("WP", "Version 5.0", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class WP: public Ui_WP {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WP_H
